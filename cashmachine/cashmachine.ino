#include <Servo.h>

#define KEY1 A5
#define KEY2 A6
#define KEY3 A7
#define KEY_OPEN A3
#define LED_OPEN 4
#define LED_SIGNAL A0
#define LOCK_PIN_POS A1
#define LOCK_PIN_NEG 7

#define VAL1 55
#define VAL2 100
#define VAL3 150
#define VAL4 200
#define VAL5 270
#define VAL6 450
#define VAL7 700
#define VAL8 800

#define DEBUG

uint8_t digits[3] = {1, 1, 1};
uint8_t digits_db[3] = {1, 1, 1};
uint8_t digits_ok[3] = {5, 2, 4};
uint8_t digits_reset[3] = {6, 6, 6};
bool opened = false;

uint8_t raw_digits[3][10];
const uint8_t key_pins[3] = {A5, A6, A7};
uint16_t values[3] = {0, 0, 0};

// углы поворота 
const uint8_t angles1[6] = {0,34,70,105,142,175};
const uint8_t angles2[6] = {180,158,118,77,39,1};
const uint8_t angles3[6] = {179,146,111,75,37,6};


byte index = 0;

Servo servo1;
Servo servo2;
Servo servo3;


void setup() {
  // put your setup code here, to run once:
//  pinMode(key_pins[0], INPUT);
//  pinMode(key_pins[1], INPUT);
//  pinMode(key_pins[2], INPUT);
  pinMode(KEY_OPEN, INPUT);
  pinMode(LED_OPEN, OUTPUT);
  pinMode(LED_SIGNAL, OUTPUT);
  pinMode(LOCK_PIN_POS, OUTPUT);
  pinMode(LOCK_PIN_NEG, OUTPUT);  

  for (byte i = 0; i < 3; i++) {
    for (byte j = 0; j < 10; j++) {
      raw_digits[i][j] = 1;
    }
  }

  digitalWrite(LED_OPEN, LOW);
  digitalWrite(LED_SIGNAL, LOW);
  digitalWrite(LOCK_PIN_POS, LOW);
  digitalWrite(LOCK_PIN_NEG, HIGH);  

  servo1.attach(3);
  servo2.attach(5);
  servo3.attach(6);

  servo1.write(angles1[0]);
  delay(50);
  servo2.write(angles2[0]);
  delay(50);
  servo3.write(angles3[0]);
  delay(50);

  // testServo();

  #ifdef DEBUG
  Serial.begin(115200);
  Serial.println("Go");
  #endif
}

void loop() {
  // put your main code here, to run repeatedly:
  
  for (byte i = 0; i < 3; i++) {
    byte d = 0;
    int v = 0;
    v = analogRead(key_pins[i]);
    raw_digits[i][index] = getDigit(v);
    d = raw_digits[i][index];
    for (byte j = 1; j < 10; j++) {
      if (raw_digits[i][j-1] != raw_digits[i][j])
        d = 0;    
    }
    if (d > 0)
      digits[i] = d;      
    #ifdef DEBUG
    Serial.print(v);
    Serial.print("\t");
    #endif
  }    
  if (index < 9)
    index++;
  else
    index = 0;    
  delay(1);
  if (!opened) {
    servo1.write(angles1[digits[0]-1]);
    servo2.write(angles2[digits[1]-1]);
    servo3.write(angles3[digits[2]-1]);
  }
  #ifdef DEBUG
  Serial.print(digits[0]);
  Serial.print(" ");
  Serial.print(digits[1]);
  Serial.print(" ");
  Serial.println(digits[2]);
  #endif
  

  if (digitalRead(KEY_OPEN) == 0) {
    delay(10);
    if (digitalRead(KEY_OPEN) == 0) {
      if (digits[0] == digits_reset[0] && digits[1] == digits_reset[1] && digits[2] == digits_reset[2]) {      
        opened = false;
        digitalWrite(LED_OPEN, LOW);
        digitalWrite(LOCK_PIN_POS, LOW);
        digitalWrite(LOCK_PIN_NEG, HIGH);          
      }      
      if (digits[0] == digits_ok[0] && digits[1] == digits_ok[1] && digits[2] == digits_ok[2]) {
        opened = true;
        delay(500);
//        for (uint8_t i = 0; i < 10; i++) {
//          digitalWrite(LED_SIGNAL, HIGH);
//          delay(100);
//          digitalWrite(LED_SIGNAL, LOW);
//          delay(100);
//        }
        digitalWrite(LED_OPEN, HIGH);
        digitalWrite(LOCK_PIN_POS, HIGH);
        digitalWrite(LOCK_PIN_NEG, LOW);  
        #ifdef DEBUG
        Serial.println("Open!");
        #endif
      }
      else {
        digits[0] = 1;
        digits[1] = 1;
        digits[2] = 1;
        delay(500);
//        for (uint8_t i = 0; i < 3; i++) {
//          digitalWrite(LED_SIGNAL, HIGH);
//          delay(400);
//          digitalWrite(LED_SIGNAL, LOW);
//          delay(400);
//        } 
        #ifdef DEBUG
        Serial.println("Closed!");
        #endif        
      }

    }
  }

}

uint8_t getDigit(int value) {
  if (value > VAL6) return 6;
  if (value > VAL5) return 5;
  if (value > VAL4) return 4;
  if (value > VAL3) return 3;
  if (value > VAL2) return 2;
  if (value > VAL1) return 1;
//if (value < VAL1) 
  return 0;
}

void testServo() {

  servo1.write(180);
  servo2.write(0);
  servo3.write(0);
  delay(500);
  for (byte i = 1; i <= 6; i++) {
    servo1.write(30 * (5 - i));
    delay(100);
    servo2.write(30 * i);
    delay(100);
    servo3.write(30 * i);
    delay(500);
  }
  servo1.write(180);
  servo2.write(0);
  servo3.write(0);  
  
}
