#include <RS485_q.h>

#define IN_1 4
#define IN_2 5
#define IN_3 6
#define IN_4 7
#define LOCK_PIN 3

#define RESET_TIME 15000

#define RX_PIN 8
#define TX_PIN 10
#define EN_PIN 9

byte * rs485data;
const byte deviceName[] = "tools.";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdLock[] =   "lock..";
const byte cmdUnlock[] = "unlock";
const byte cmdReset[] =  "reset.";
const byte cmdReboot[] = "reboot";
byte reply[6];

bool isLocked = true;
bool timerFlag = false;

uint8_t stateMask = 0;
uint8_t prevStateMask = 0;
uint8_t toolPins[4] = {7, 5, 6, 4}; // так надо
uint8_t toolPokes[4] = {0, 0, 0, 0};
uint8_t toolPokesOpen[4] = {5, 4, 3, 2};
uint8_t toolPokesReset[4] = {2, 2, 2, 2};
uint8_t toolPinsDebounce[4] = {0, 0, 0, 0};
uint8_t * toolPokesNeeded;
uint32_t lastPokeTime = 0, timeDiff = 0;
uint8_t lastPokeNum = 5; // invalid index, yes

/* 
* -------------- main -----------
*/

void setup() {
  pinMode(IN_1, INPUT);
  pinMode(IN_2, INPUT);
  pinMode(IN_3, INPUT);
  pinMode(IN_4, INPUT);
  pinMode(LOCK_PIN, OUTPUT);

  Serial.begin(115200);
  Serial.println("tools start");

  //Установка Таймера2.
  TCCR2A = 0;
  TCCR2B = 1<<CS22 | 1<<CS21 | 1<<CS20; // 111 = /1024
  TIMSK2 = 1<<TOIE2; //Подключение прерывания по переполнению Timer2

  resetGame();

  rs485.begin();
}

void loop() {
  if (true == timerFlag) {
    timerFlag = false;

    if (isLocked)
      toolPokesNeeded = toolPokesOpen;
    else
      toolPokesNeeded = toolPokesReset;

    timeDiff = millis() - lastPokeTime;
    if (timeDiff >= RESET_TIME)
      resetPokes();
    checkInputs();

//    if (stateMask != prevStateMask)
      debugInfo();
  }

  if (rs485.update()) {
    rs485data = rs485.getData();
    #ifdef DEBUG
    Serial.print("Got data: ");
    Serial.write(rs485data, 6);
    Serial.println();
    #endif
    if (compareCmd(rs485data, cmdLock)) {
      lockOn();
      resetPokes();
    }
    else if (compareCmd(rs485data, cmdUnlock)) {
      lockOff();
      resetPokes();
    }    

    if (compareCmd(rs485data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else {
      constructReply();
      rs485.sendMsg((const byte*) reply, 6);
    }
  rs485.reset();
  }  
  
}

ISR(TIMER2_OVF_vect) {
  timerFlag = true;
}

/* 
* -------------- func -----------
*/
void resetGame() {
  lockOn();
  resetPokes();
}

inline void lockOn() {
  digitalWrite(LOCK_PIN, LOW);
  isLocked = true;
}

inline void lockOff() {
  digitalWrite(LOCK_PIN, HIGH);
  isLocked = false;
}

void checkPokes() {
  for (uint8_t i = 0; i < 4; i++) {
    if (toolPokes[i] != toolPokesNeeded[i])
      return;
  }
  Serial.println("YAy!");
  if (isLocked)
    lockOff();
  else
    lockOn();
}

void checkInputs() {
  for (uint8_t i = 0; i < 4; i++) {
    if (digitalRead(toolPins[i]) == 0) {
      if (stateMask & (1 << i)) 
        toolPinsDebounce[i]++;
      else
        toolPinsDebounce[i] = 0;
    }
    else  {
      if (stateMask & (1 << i))
        toolPinsDebounce[i] = 0;
      else
        toolPinsDebounce[i]++;
    }

    if (toolPinsDebounce[i] == 6) {
      if (stateMask & (1 << i))
        stateMask &= ~(1 << i); // write 0
      else {
        poke(i);
        stateMask |= (1 << i); // write 1
      }
    }
  }
}

void poke(uint8_t n) {
  toolPokes[n] += 1;
  if (lastPokeNum > 3) { // not in range
    lastPokeNum = n;
  }
  else if (lastPokeNum == n) {
    if (toolPokes[n] > toolPokesNeeded[n])  {
      resetPokes();
      if (n == 0)
        toolPokes[n] += 1;
    }
  }
  else {
    if ((lastPokeNum != (n-1)) || (toolPokes[lastPokeNum] != toolPokesNeeded[lastPokeNum])) {
      resetPokes();
      if (n == 0)
        toolPokes[n] += 1;
    }
  }

  lastPokeTime = millis();
  lastPokeNum = n;
  checkPokes();
}

void resetPokes() {
  for (uint8_t i = 0; i < 4; i++) {
    toolPokes[i] = 0;
  }
  lastPokeNum = 5; // not in range
}

void debugInfo() {
    Serial.print(isLocked, BIN);
    Serial.print(" ");
    Serial.print(stateMask, BIN);
    Serial.print(" ");
    Serial.print(toolPokes[0]);
    Serial.print(toolPokes[1]);
    Serial.print(toolPokes[2]);
    Serial.print(toolPokes[3]);
    Serial.print(" ");
    Serial.println(millis());
}

void constructReply() {
  
  reply[0] = ((isLocked) ? 'L' : 'U');
  reply[1] = '.';
  reply[2] = toolPokes[0] + 48; // chr(48) == '0'
  reply[3] = toolPokes[1] + 48;
  reply[4] = toolPokes[2] + 48;
  reply[5] = toolPokes[3] + 48;
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}
