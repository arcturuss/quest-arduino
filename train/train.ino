#include <RS485_q.h>

#define EN_PIN A2
#define RX_PIN A0
#define TX_PIN A1

#define SMOKE_PIN 6  // LOW = on
#define SMOKE_ON_PIN 9
#define VENT_PIN 7
#define UV_PIN 8
#define LIGHT_PIN 5

#define LAMP_PIN 2  // HIGH = on

#define SENS_PIN 10    // low-pulled, HIGH = off
#define BUTTON_PIN 4  // low-pulled, HIGH = on

byte * rs485_data;
const byte deviceName[] = "train.";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdOn[] =     "turnon";
const byte cmdOff[] =    "trnoff";
const byte cmdReset[] =  "reset.";
const byte cmdReboot[] = "reboot";
const byte cmdSmoke[] =  "smoke.";
const byte cmdSmoke2[] = "smoke2";
const byte cmdUvOn[] =   "uvon..";
const byte cmdUvOff[] =  "uvoff.";
const byte cmdPlay[] =   "sound.";
const byte cmdStop[] =   "stop..";
const byte cmdVentOn[] =     "venton";
const byte cmdVentOff[] =    "vntoff";
const byte cmdLampOn[] =     "lampon";
const byte cmdLampOff[] =    "lmpoff";
const byte cmdLightOn[] =     "lghton";
const byte cmdLightOff[] =    "lgtoff";
const char* respStatus[] = { "isoff.", "heatng", "ison..", "smokng", "uvlght"};
char reply[6];

typedef enum { off = 0, heating, on, smoking, uvlight} status_t;
status_t status = off;

uint8_t timerButtonCnt = 0;
const uint8_t timerButtonCompare = 3;
bool buttonHandled = false;

uint8_t timerSensorCnt = 0;
const uint8_t timerSensorCompare = 3;
bool sensorHandled = false;

bool trainSeqStarted = false;
bool uvSeqStarted = false;

bool smokeStatus = 0;
bool smokeOnStatus = 0;
bool ventStatus = 0;
bool uvStatus = 0;
bool lampStatus = 0;
bool lightStatus = 1;
bool preheatStatus = 0; // 1 = heated, 0 = off or waiting
bool smokeAfterPreheat = false;
bool smokePaused = false;

// flags
bool startTrainSeqFlag = false;
bool startUvSeqFlag = false;

uint16_t timerPreheatCnt = 0;
const uint16_t timerPreheatCompare = 61*60; // not 10 sec
uint16_t timerPostheatCnt = 0;
const uint16_t timerPostheatCompare = 61*30; // 30 sec
uint8_t timerSmokeOnCnt = 0;
const uint8_t timerSmokeOnCompare = 240; // ~4 sec
uint16_t timerSmokeDelayCnt = 0;
uint16_t timerSmokeDelayCompare = 0; // not const, dynamic
uint8_t timerAfterSmokeDelayCnt = 0;
const uint8_t timerAfterSmokeDelayCompare = 184; // 3 sec
uint16_t timerVentOffDelayCnt = 0;
const uint16_t timerVentOffDelayCompare = 61*30; // 30 sec
uint16_t timerLampOffDelayCnt = 0;
uint16_t timerLampOffDelayCompare = 0; // not const, dynamic

uint16_t timerUvOnCnt = 0;
const uint16_t timerUvOnCompare = 610; // 10 sec

void setup() {
  // put your setup code here, to run once:

  pinMode(SMOKE_PIN, OUTPUT);
  pinMode(SMOKE_ON_PIN, OUTPUT);
  pinMode(VENT_PIN, OUTPUT);
  pinMode(UV_PIN, OUTPUT);
  pinMode(LAMP_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);

  digitalWrite(SMOKE_PIN, HIGH);
  digitalWrite(SMOKE_ON_PIN, HIGH);
  digitalWrite(VENT_PIN, HIGH);
  digitalWrite(UV_PIN, HIGH);
  digitalWrite(LAMP_PIN, LOW);
  digitalWrite(LIGHT_PIN, HIGH);

  pinMode(SENS_PIN, INPUT);
  pinMode(BUTTON_PIN, INPUT);

  Serial.begin(115200);
  
  startupTest();

  //Установка Таймера2.
  TCCR2A = 0;
  TCCR2B = 1 << CS22 | 1 << CS21 | 1 << CS20; // 111 = /1024 = 15625 Hz
  //Подключение прерывания по переполнению Timer2
  TIMSK2 = 1 << TOIE2;  // 15625 / 256 = 61 Hz  

  interrupts(); 
  rs485.begin();
  Serial.println("train start");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (startTrainSeqFlag) {
    startTrainSeqFlag = false;
    startTrainSeq();
  }
  if (startUvSeqFlag) {
    startUvSeqFlag = false;
    startUvSeq();
  }  

  if (rs485.update()) {
    rs485_data = rs485.getData();
    #ifdef DEBUG
    Serial.print("Got data: ");
    Serial.write(rs485_data, 6);
    Serial.println();
    #endif
    // if (compareCmd(rs485_data, cmdStatus)) {
    //   rs485.sendMsg((const byte*)respStatus[status], 6);
    // }
    if (compareCmd(rs485_data, cmdOn)) {
      // status = on; // status is set inside function
      turnOn();
    }
    else if (compareCmd(rs485_data, cmdOff)) {
      status = off;
      turnOff();
    }
    else if (compareCmd(rs485_data, cmdVentOn)) {
      ventOn();
    }
    else if (compareCmd(rs485_data, cmdVentOff)) {
      ventOff();
    }      
    else if (compareCmd(rs485_data, cmdLampOn)) {
      lampOn();
    }
    else if (compareCmd(rs485_data, cmdLampOff)) {
      lampOff();
    }       
    else if (compareCmd(rs485_data, cmdLightOn)) {
      lightOn();
    }
    else if (compareCmd(rs485_data, cmdLightOff)) {
      lightOff();
    }                 
    else if (compareCmd(rs485_data, cmdSmoke)) {
      if ((status != off) && (status != heating))
        startTrainSeq();
//      status = on;
    }
    else if (compareCmd(rs485_data, cmdSmoke2)) {
      if ((status != off) && (status != heating))
        smokeNow();
//      status = on;
    }    
    else if (compareCmd(rs485_data, cmdUvOn)) {
      uvOn();
      lightOff();
      lampOff(); // just in case
      status = uvlight;
    }
    else if (compareCmd(rs485_data, cmdUvOff)) {
      uvOff();
      lightOn();
      if (preheatStatus)
        status = on;
      else if (smokeStatus)
        status = heating;
      else 
        status = off;
    }

    if (compareCmd(rs485_data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else {
      // rs485.sendMsg((const byte*)respStatus[status], 6);
      constructReply();
      rs485.sendMsg((const byte*)reply, 6);
    }

  rs485.reset();
  }  
  
}

void constructReply() {
  if (smokeStatus)
    if (preheatStatus)
      reply[0] = '2'; // heated
    else
      reply[0] = '1'; // heating
  else
    reply[0] = '0'; // off
  reply[1] = (smokeOnStatus) ? '1' : '0';
  reply[2] = (lampStatus)    ? '1' : '0';
  reply[3] = (ventStatus)    ? '1' : '0';
  reply[4] = (uvStatus)      ? '1' : '0';
  reply[5] = (lightStatus)   ? '1' : '0';
}

//
// ------------------------------------------------------------------------------
//

void turnOn() {
  smokeEnable();
  // test preheating status here
  status = heating;
  smokePreheat();
  smokeAfterPreheat = false;
}

void turnOff() {
  smokeOff();
  smokeDisable();
  if (status <= on) // on or heating
    status = off;
}

void resetGame() {
  noInterrupts();

  digitalWrite(SMOKE_PIN, HIGH);
  digitalWrite(SMOKE_ON_PIN, HIGH);
  digitalWrite(VENT_PIN, HIGH);
  digitalWrite(UV_PIN, HIGH);
  digitalWrite(LAMP_PIN, LOW);
  digitalWrite(LIGHT_PIN, HIGH);

  timerButtonCnt = 0;
  buttonHandled = false;

  timerSensorCnt = 0;
  sensorHandled = false;

  smokeStatus = 0;
  smokeOnStatus = 0;
  ventStatus = 0;
  uvStatus = 0;
  lampStatus = 0;
  lightStatus = 1;
  preheatStatus = 0; // 1 = heated, 0 = off or waiting
  smokeAfterPreheat = false;
  smokePaused = false;

// flags
  startTrainSeqFlag = false;  
  startUvSeqFlag = false;  

  trainSeqStarted = false;
  uvSeqStarted = false;  

  timerPreheatCnt = 0;
  timerPostheatCnt = 0;
  timerSmokeOnCnt = 0;
  timerSmokeDelayCnt = 0;
  timerSmokeDelayCompare = 0; // not const, dynamic
  timerAfterSmokeDelayCnt = 0;
  timerVentOffDelayCnt = 0;
  timerLampOffDelayCnt = 0;
  timerLampOffDelayCompare = 0; // not const, dynamic
  timerUvOnCnt = 0;

  status = off;

  interrupts();
}

void startupTest() {
  smokeEnable();
  delay(100);
  smokeDisable();
  delay(100);
  ventOn();
  delay(100);
  ventOff();
  delay(100);
  uvOn();
  delay(100);
  uvOff();
  delay(100);
  smokeOn();
  delay(100);
  smokeOff();
  delay(100);
  lightOff();
  delay(100);
  lightOn();
  delay(100);
  lampOn();
  delay(100);
  lampOff();
  delay(100);
}

//
// ------------------------------------------------------------------------------
// Timer2 overflow interrupt vector handler
//
ISR(TIMER2_OVF_vect) {
  
/*
  // button
  if (digitalRead(BUTTON_PIN) == HIGH) {
    if ((timerButtonCnt >= timerButtonCompare) && !buttonHandled) {
      if (!trainSeqStarted)
        //startTrainSeq();
        startTrainSeqFlag = true; // do not call heavy functions in the interrupt
      buttonHandled = true;
      timerButtonCnt = 0;
    }
    else {
      timerButtonCnt++;
    }
  }
  else {
    timerButtonCnt = 0;
    buttonHandled = false;
  }

  // sensor (currently button2)
  if (digitalRead(SENS_PIN) == LOW) {
    if ((timerSensorCnt >= timerSensorCompare) && !sensorHandled) {
      if (!uvSeqStarted)
        //startTrainSeq();
        startUvSeqFlag = true; // do not call heavy functions in the interrupt
      sensorHandled = true;
      timerSensorCnt = 0;
    }
    else {
      timerSensorCnt++;
    }
  }
  else {
    timerSensorCnt = 0;
   sensorHandled = false;
  }  

*/  

  // preheat
  if (timerPreheatCnt > 0) {
    if (timerPreheatCnt >= timerPreheatCompare) {
      if (status == heating)
        status = on;
      preheatStatus = 1;
      timerPreheatCnt = 0;
      // timerPostheatCnt = 1; // start OFF timer
      if (smokeAfterPreheat) {
        //smokeNow();
        startTrainSeqFlag = 1;
      }
    }
    else {
      timerPreheatCnt++;
    }
  }

  // smokeOn
  if (timerSmokeOnCnt > 0) {
    if (timerSmokeOnCnt >= timerSmokeOnCompare) {
      smokeOff();
      timerSmokeOnCnt = 0;
      timerPostheatCnt = 1; // REstart OFF timer
      // smokePaused = true;
      timerAfterSmokeDelayCnt = 1; // start 'deadtime' between smokes
      timerVentOffDelayCnt = 1; // start VENT OFF timer
    }
    else {
      timerSmokeOnCnt++;
    }
  }

  // after smoke delay
  if (timerAfterSmokeDelayCnt > 0) {
    if (timerAfterSmokeDelayCnt >= timerAfterSmokeDelayCompare) {
      timerAfterSmokeDelayCnt = 0;
      smokePaused = false;
      trainSeqStarted = false;
      if (status == smoking)
        status = on;
    }
    else {
      timerAfterSmokeDelayCnt++;
    }
  }

  /* temporary disabled */
  // if (timerPostheatCnt > 0) {
  //   if (timerPostheatCnt >= timerPostheatCompare) {
  //     timerPostheatCnt = 0;
  //     smokeOff(); // just in case
  //     smokeDisable();
  //     preheatStatus = 0;
  //   }
  //   else {
  //     timerPostheatCnt++;
  //   }
  // }  

  /*
  if (timerVentOffDelayCnt > 0) {
    if (timerVentOffDelayCnt >= timerVentOffDelayCompare) {
      timerVentOffDelayCnt = 0;
      ventOff();
    }
    else {
      timerVentOffDelayCnt++;
    }
  }    
  */

  if (timerLampOffDelayCnt > 0) {
    if (timerLampOffDelayCnt >= timerLampOffDelayCompare) {
      timerLampOffDelayCnt = 0;
      lampOff();
    }
    else {
      timerLampOffDelayCnt++;
    }
  }      

  if (timerSmokeDelayCnt > 0) {
    if (timerSmokeDelayCnt >= timerSmokeDelayCompare) {
      timerSmokeDelayCnt = 0;
      smokeNow();
    }
    else {
      timerSmokeDelayCnt++;
    }
  }        

  if (timerUvOnCnt > 0) {
    if (timerUvOnCnt >= timerUvOnCompare) {
      timerUvOnCnt = 0;
      uvOff();
      lightOn();
    }
    else {
      timerUvOnCnt++;
    }
  }      
}

//
// ------------------------------------------------------------------------------
//

void startTrainSeq() {
  // if (!preheatStatus) {
  //   smokePreheat();
  //   smokeAfterPreheat = true;
  // }
  // else {
    trainSeqStarted = true;
    status = smoking;
    lampOn();
    lampOffDelay(6000); // ms
    // ventOn(); // goes off after smoke
    // smokeNow();
    smokeDelay(1000); //ms
  // }
}

void smokeDelay(uint16_t delayTime) {
  delayTime /= 16; //приблизительно
  timerSmokeDelayCompare = delayTime;
  timerSmokeDelayCnt = 1;
}

void smokeNow() {
  if (smokePaused)  {
    trainSeqStarted = false; // костыль 
    return;
  }
  if (smokeStatus && !smokeOnStatus) {
    smokeOn();
    timerSmokeOnCnt = 1;
  }
}

void smokePreheat() {
  if (!smokeStatus)
    smokeEnable();  
  if (preheatStatus) {
    status = on;
    return;
  }

 // if (smokeStatus && (timerPreheatCnt == 0)) {
    timerPreheatCnt = 1; // start timer only once, do not restart
 // }  
}

void lampOffDelay(uint16_t delayTime) {
  // delayTime /= 16; //приблизительно
  timerLampOffDelayCompare = (delayTime / 16);
  timerLampOffDelayCnt = 1;
}

void startUvSeq() {
  uvOn();
  lightOff();
  timerUvOnCnt = 1;
}

//
// ------------------------------------------------------------------------------
// On-Off functions

inline void smokeEnable() {
  digitalWrite(SMOKE_PIN, LOW);
  smokeStatus = 1;
}

inline void smokeDisable() {
  digitalWrite(SMOKE_PIN, HIGH);
  smokeStatus = 0;
}

inline void smokeOn() {
  digitalWrite(SMOKE_ON_PIN, LOW);
  smokeOnStatus = 1;
}

inline void smokeOff() {
  digitalWrite(SMOKE_ON_PIN, HIGH);
  smokeOnStatus = 0;
}

inline void ventOn() {
  digitalWrite(VENT_PIN, LOW);
  ventStatus = 1;
}

inline void ventOff() {
  digitalWrite(VENT_PIN, HIGH);
  ventStatus = 0;
}

inline void uvOn() {
  digitalWrite(UV_PIN, LOW);
  uvStatus = 1;
}

inline void uvOff() {
  digitalWrite(UV_PIN, HIGH);
  uvStatus = 0;
}

inline void lampOn() {
  digitalWrite(LAMP_PIN, HIGH);
  lampStatus = 1;
}

inline void lampOff() {
  digitalWrite(LAMP_PIN, LOW);
  lampStatus = 0;
}

inline void lightOn() {
  digitalWrite(LIGHT_PIN, HIGH);
  lightStatus = 1;
}

inline void lightOff() {
  digitalWrite(LIGHT_PIN, LOW);
  lightStatus = 0;
}

//
//-----------------------

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}
