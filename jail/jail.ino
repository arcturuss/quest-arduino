#include <RS485_q.h>
#include <EEPROM.h>
#include <SPI.h>
#include <MFRC522.h>

#define LOCK_PIN 2
#define VENT_PIN 4

#define RX_PIN A0
#define TX_PIN A1
#define EN_PIN A2

#define SS_PIN 10
#define RS_PIN 9
#define RFID_WIPE_PIN 7

#define DEBUG

MFRC522 reader(SS_PIN, RS_PIN);
void(*resetFunc)(void) = 0;

byte readCard[4];   // Stores scanned ID read from RFID Module
byte firstMasterCard[4];   // Stores master card's ID read from EEPROM
byte secondMasterCard[4];   // Stores master card's ID read from EEPROM
byte gameCard[4];

bool equalsMaster = false;
uint8_t successRead = 0;    // Variable integer to keep if we have Successful Read from Reader
uint8_t mode = 0;
uint64_t lastTimeMs = 0;
uint64_t wipeTime = 10000;

byte * rs485data;
const byte deviceName[] = "jail..";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdUnlock[] = "unlock";
const byte cmdLock[] = "lock..";
const byte cmdReset[] = "reset.";
const byte cmdReboot[] = "reboot";
const byte cmdVentOn[] = "venton";
const byte cmdVentOff[] = "vntoff";

byte reply[6];

byte ventStatus = 0;
byte lockStatus = 1;
byte cardStatus = 0;

void setup() {
  pinMode(RFID_WIPE_PIN, INPUT_PULLUP);
  pinMode(LOCK_PIN, OUTPUT);
  pinMode(VENT_PIN, OUTPUT);
  lockOn();
  ventOff();

  Serial.begin(115200);  // Initialize serial communications with PC
  SPI.begin();           // MFRC522 Hardware uses SPI protocol
  reader.PCD_Init();
  reader.PCD_SetAntennaGain(reader.RxGain_43dB);
  Serial.println(F("RFID MFRC522 MODULE V1.0"));   // For debugging purposes
  ShowReaderDetails();

  if (EEPROM.read(1) != 127) {
    initCards();
  }
  dumpCardInfo();
  Serial.println(F("Everything is ready"));
  Serial.println(F("Waiting PICCs to be scanned"));

  rs485.begin();
}

void loop() {
  processWipeButton();
  reader.PCD_Init(); //re-init
  for (byte i = 0; i < 4; i++) {
    readCard[i] = 0;
  }
  getID();

  //add PICC to EEPROM
  if (cardEquals(readCard, firstMasterCard) || cardEquals(readCard, secondMasterCard)) {
    Serial.println(F("Master Card Scanned"));
    Serial.println(F("Scan a PICC to ADD to EEPROM"));
    scanGameCard();
  }

  if (cardEquals(readCard, gameCard)) {
//    Serial.println(F("Game Card Scanned"));
    cardStatus = 1;
//    for (byte i = 0; i < 4; i++) {
//      readCard[i] = 0;
//    }
  }
  else {
    cardStatus = 0;
  }

  if (rs485.update()) {
    rs485data = rs485.getData();
    
    Serial.print("Got data: ");
    Serial.write(rs485data, 6);
    Serial.println();
    

    if (compareCmd(rs485data, cmdUnlock)) {
      lockOff();
    }
    else if (compareCmd(rs485data, cmdLock)) {
      lockOn();
    }
    else if (compareCmd(rs485data, cmdVentOn)) {
    ventOn();
    }
    else if (compareCmd(rs485data, cmdVentOff)) {
      ventOff();
    }    

    if (compareCmd(rs485data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else {
      // if (compareCmd(rs485data, cmdStatus)) {
      constructReply();
      rs485.sendMsg((const byte*) reply, 6);
    }
    rs485.reset();
  }

}

void constructReply() {
  reply[0] = 'v';
  reply[1] = ((ventStatus) ? '1' : '0');
  reply[2] = 'l';
  reply[3] = ((lockStatus) ? '1' : '0');
  reply[4] = 'c';
  reply[5] = ((cardStatus) ? '1' : '0');
}

void resetGame() {
  ventOff();
  lockOn();
  cardStatus = 0;

  reader.PCD_Init();
}

void initCards() {
    Serial.println(F("Define Cards"));

    Serial.println(F("Scan A PICC to Define as First Master Card"));
    scanFirstMasterCard();

    Serial.println(F("Scan A PICC to Define as Second Master Card"));
    scanSecondMasterCard();

    Serial.println(F("Scan a PICC to ADD to EEPROM"));
    scanGameCard();

    EEPROM.write(1, 127);                  // Write to EEPROM we defined Master Cards.
    Serial.println(F("-------------------"));
}

void dumpCardInfo() {
  Serial.println(F("First Master Card's UID"));

  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    Serial.print(firstMasterCard[i] = EEPROM.read(2 + i), HEX);
  }
  Serial.println("");

  Serial.println(F("Second Master Card's UID"));
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    Serial.print(secondMasterCard[i] = EEPROM.read(6 + i), HEX);
  }
  Serial.println("");

  Serial.println(F("Game Card's UID"));
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    Serial.print(gameCard[i] = EEPROM.read(10 + i), HEX);
  }
  Serial.println("");
  Serial.println(F("-------------------"));
}

inline void lockOn() {
  digitalWrite(LOCK_PIN, HIGH);
  lockStatus = 1;
}

inline void lockOff() {
  digitalWrite(LOCK_PIN, LOW);
  lockStatus = 0;
}

inline void ventOn() {
  digitalWrite(VENT_PIN, HIGH);
  ventStatus = 1;
}

inline void ventOff() {
  digitalWrite(VENT_PIN, LOW);
  ventStatus = 0;
}

///////////////////////////////////////// Get PICC's UID ///////////////////////////////////
uint8_t getID() {
  // Getting ready for Reading PICCs
  // if (!reader.PICC_IsNewCardPresent() and !reader.PICC_IsOldCardPresent()) { //If a new PICC placed to RFID reader continue
  //   return 0;
  // }
  if (!reader.PICC_IsNewCardPresent()) {
    return 0;
  }
  if (!reader.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.println(F("Scanned PICC's UID:"));
  for ( uint8_t i = 0; i < 4; i++) {  //
    readCard[i] = reader.uid.uidByte[i];
    Serial.print(readCard[i], HEX);
  }
  Serial.println("");
  reader.PICC_HaltA(); // Stop reading
  return 1;
}

bool cardEquals(byte a[], byte b[]) {
  for (uint8_t k = 0; k < 4; k++) {   // Loop 4 times
    if (a[k] != b[k])     // IF a != b then set match = false, one fails, all fail
      return false;
  }
  return true;      // Return true
}

bool processWipeButton() {
  if (!digitalRead(RFID_WIPE_PIN)) {
    if (lastTimeMs + wipeTime < millis())
    {
      wipeEEPROM();
    }
  } else {
    lastTimeMs = millis();
  }
}

void wipeEEPROM() {
  Serial.println(F("Starting Wiping EEPROM"));
  for (uint16_t x = 0; x < EEPROM.length(); x = x + 1) {    //Loop end of EEPROM address
    if (EEPROM.read(x) == 0) {              //If EEPROM address 0
      // do nothing, already clear, go to the next address in order to save time and reduce writes to EEPROM
    }
    else {
      EEPROM.write(x, 0);       // if not write 0 to clear, it takes 3.3mS
    }
  }
  Serial.println(F("EEPROM Successfully Wiped"));
  resetFunc();
}
void scanFirstMasterCard() {
  do {
    successRead = getID();            // sets successRead to 1 when we get read from reader otherwise 0
  } while (!successRead);                  // Program will not go further while you not get a successful read

  writeID(readCard, 2);
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    firstMasterCard[i] = EEPROM.read(2 + i);    // Write it to masterCard
  }
  Serial.println(F("First Master Card Defined"));
}

void scanSecondMasterCard() {
  equalsMaster = false;
  do {
    do {
      successRead = getID();            // sets successRead to 1 when we get read from reader otherwise 0
    } while (!successRead);                  // Program will not go further while you not get a successful read

    equalsMaster = cardEquals(firstMasterCard, readCard);
    if (!equalsMaster) {
      writeID(readCard, 6);
      for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
        secondMasterCard[i] = EEPROM.read(6 + i);    // Write it to masterCard
      }
      Serial.println(F("Second Master Card Defined"));
    } else {
      Serial.println(F("It is a first Master Card"));
      Serial.println(F("Scan another PICC to Define as Second Master Card"));
    }
  } while (equalsMaster);
}


void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = reader.PCD_ReadRegister(reader.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown),probably a chinese clone?"));
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
    Serial.println(F("SYSTEM HALTED: Check connections."));
    while (true); // do not go further
  }
}

void scanGameCard() {
  equalsMaster = false;
  do {
    do {
      successRead = getID();  // sets successRead to 1 when we get read from reader otherwise 0
    } while (!successRead);   //the program will not go further while you are not getting a successful read

    if (cardEquals(readCard, firstMasterCard)) {
      Serial.println(F("It is a first Master Card"));
      Serial.println(F("Scan another PICC to Define as Game Card"));
      equalsMaster = true;
    } else if (cardEquals(readCard, secondMasterCard)) {
      Serial.println(F("It is a second Master Card"));
      Serial.println(F("Scan another PICC to Define as Game Card"));
      equalsMaster = true;
    } else {
      writeID(readCard, 10);
      for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
        gameCard[i] = EEPROM.read(10 + i);    // Write it to gameCard
      }
      Serial.println(F("Game Card Defined"));
      equalsMaster = false;
    }
  } while (equalsMaster);
}

void writeID(byte a[], uint8_t start) {
  for ( uint8_t j = 0; j < 4; j++ ) {   // Loop 4 times
    EEPROM.write( start + j, a[j] );  // Write the array values to EEPROM in the right position
  }
}

void delay_ms(unsigned long ms) {
  uint16_t start = static_cast<uint16_t>(micros());

  while (ms > 0) {
    if ((static_cast<uint16_t>(micros()) - start) >= 1000) {
      ms--;
      start += 1000;
    }
  }
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}
