/*
 RS485 protocol library - non-blocking.

 Devised and written by Nick Gammon.
 Date: 4 December 2012
 Version: 1.0

 Can send from 1 to 255 bytes from one node to another with:

 * Packet start indicator (STX)
 * Each data byte is doubled and inverted to check validity
 * Packet end indicator (ETX)
 * Packet CRC (checksum)


 To allow flexibility with hardware (eg. Serial, SoftwareSerial, I2C)
 you provide three "callback" functions which send or receive data. Examples are:

 size_t fWrite (const byte what)
   {
   return Serial.write (what);
   }

 int fAvailable ()
   {
   return Serial.available ();
   }

 int fRead ()
   {
   return Serial.read ();
   }


 PERMISSION TO DISTRIBUTE

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.


 LIMITATION OF LIABILITY

 The software is provided "as is", without warranty of any kind, express or implied,
 including but not limited to the warranties of merchantability, fitness for a particular
 purpose and noninfringement. In no event shall the authors or copyright holders be liable
 for any claim, damages or other liability, whether in an action of contract,
 tort or otherwise, arising from, out of or in connection with the software
 or the use or other dealings in the software.

 */


#include <RS485_q.h>



// allocate the requested buffer size
void RS485::begin ()
  {
  serial_.begin(38400);
  pinMode(enablePin_, OUTPUT);
  
  data_ = (byte *) malloc (bufferSize_);
  reset ();
  errorCount_ = 0;
  } // end of RS485::begin

// get rid of the buffer
void RS485::stop ()
{
  reset ();
  free (data_);
  data_ = NULL;
} // end of RS485::stop

// called after an error to return to "not in a packet"
void RS485::reset ()
  {
  haveSTX_ = false;
  haveName_ = true;
  available_ = false;
  inputPos_ = 0;
  startTime_ = 0;
  } // end of RS485::reset

// calculate 8-bit CRC
byte RS485::crc8 (const byte *addr, byte len)
{
  byte crc = 0;
  while (len--)
    {
    byte inbyte = *addr++;
    for (byte i = 8; i; i--)
      {
      byte mix = (crc ^ inbyte) & 0x01;
      crc >>= 1;
      if (mix)
        crc ^= 0x8C;
      inbyte >>= 1;
      }  // end of for
    }  // end of while
  return crc;
}  // end of RS485::crc8

// send a message of "length" bytes (max 255) to other end
// put STX at start, ETX at end, and add CRC
// -- rewrited to send plain bytes without ETX
//    and with double STX
void RS485::sendMsg (const byte * data, const byte length)
{
  // no callback? Can't send
  // if (fWriteCallback_ == NULL)
  //   return;

  digitalWrite(enablePin_, HIGH);

  serial_.write(ETX);  // ETX (not STX, inverted)
  serial_.write(ETX);  // two times, yes
  for (byte i = 0; i < 6; i++) {
    serial_.write(deviceName_[i]);
    data_[i] = deviceName_[i];
  }
  for (byte i = 0; i < length; i++) {
    serial_.write(data[i]);
    data_[i+6] = data[i];
  }
  serial_.write(crc8(data_, length+6)); 

  delayMicroseconds(1200);
  digitalWrite(enablePin_, LOW);
}  // end of RS485::sendMsg

// called periodically from main loop to process data and
// assemble the finished packet in 'data_'

// returns true if packet received.

// You could implement a timeout by seeing if isPacketStarted() returns
// true, and if too much time has passed since getPacketStartTime() time.

// -- rewrited to receive plain bytes instead of complemented
//    and without ETX
// TODO: implement both methods, choose using ifdef 

bool RS485::update ()
  {
  // no data? can't go ahead (eg. begin() not called)
  if (data_ == NULL)
    return false;

  // no callbacks? Can't read
  // if (fAvailableCallback_ == NULL || fReadCallback_ == NULL)
  //   return false;

  while (serial_.available() > 0)
    {
    byte inByte = serial_.read ();
    #ifdef RS485_DEBUG
    Serial.write(inByte);
    #endif
    switch (inByte)
      {

        case STX:   // start of text - check for double STX
          if (inputPos_ == 0) {
            inputPos_++;
          } else {
            haveSTX_ = true;
            inputPos_ = 0;
            startTime_ = millis ();
            #ifdef RS485_DEBUG
            Serial.println('STX');
            #endif            
          }
          break;


        default:
          // wait until packet officially starts
          if (!haveSTX_)
            break;

          if (inputPos_ == bufferSize_ && haveName_) {
            if (crc8 (data_, bufferSize_) != inByte) {
              reset ();
              errorCount_++;
              #ifdef RS485_DEBUG
              Serial.println('CRC');
              #endif                 
              break;  // bad crc
            } // end of bad CRC

            available_ = true;
            return true;  // show data ready
          }  // end if have ETX already

          if (inputPos_ < 6) {
            if (inByte != deviceName_[inputPos_]) {
              haveName_ = false;
              // reset();
              break;
            }
          }

          // keep adding if not full
          if (inputPos_ < bufferSize_)
            data_ [inputPos_++] = inByte;
          else {
            reset (); // overflow, start again
            errorCount_++;
            #ifdef RS485_DEBUG
            Serial.print('E');
            Serial.println(errorCount_);
            #endif
          }

          break;

      }  // end of switch
    }  // end of while incoming data

  return false;  // not ready yet
  } // end of RS485::update

