/*
 * WARNING!
 * define FAILURE_HANDLING in RF24_config.h
 * 
 */

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <RS485_q.h>
#include <avr/wdt.h>

#define RX_PIN A0
#define TX_PIN A2
#define EN_PIN A1

byte * rs485data;
const byte deviceName[] = "light.";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdLightOn[] =     "lghton";
const byte cmdLightOff[] =    "lgtoff";
const byte cmdReset[] =  "reset.";
const byte cmdReboot[] = "reboot";
const byte cmdArrow[] =  "arrow.";
const byte cmdLightCancel[] = "lgtcnc";

//const char* respStatus[] = { "isoff.", "ison..", "vhigh.",
//                             "vmid..", "vlow..", "voff..",
//                             "cards.", "rferr.", "error."
//                             };
typedef enum { off = 0, on, vhigh, vmid, vlow, voff, cards, linkerr, rferr, error} status_t;
status_t status = off;
byte reply[6];

uint8_t lastcard = 9; // 0 = ok, 1-4 = card, 9 = none

const uint8_t voltageHigh = 204;
const uint8_t voltageMid  = 188;
const uint8_t voltageLow  = 168;

RF24 radio(9, 10);

bool radioNumber = 1; // 0 = flashlight, 1 = arrow
//const uint64_t pipe = 0xE8E8F0F0E1LL; // Single radio pipe address for the 2 nodes to communicate.
byte addresses[][6] = {"1N1de", "2N2de"}; // from example
byte counter = 1;     // A single byte to keep track of the data being sent back and forth

const uint8_t len = 3;
uint8_t data[len];
uint8_t dataOn[] = { 22, 0, 23 };
uint8_t dataOff[] = { 0, 25, 0 };
uint8_t dataReset[] = { 42, 24, 42 };
const uint8_t dataOnResp[] = { 14, 48, 15 };
const uint8_t dataOffResp[] = { 47, 13, 46 };
const uint8_t dataResetResp[] = { 54, 18, 54 };

uint8_t dataArrow[][3] = {{ 33, 11, 25 },  // ok
  {33, 12, 26 },  // H
  {33, 13, 27 },  // E
  {33, 14, 28 },  // L
  {33, 15, 29 }   // P
}; 

// Timer at 15625 / 256 = 61 Hz = 16.4 ms period
//uint8_t timerButtonCnt = 0;
//const uint8_t timerButtonCompare = 6; // 82 ms = (16.4*(6-1))
uint8_t timerArrowCnt = 0;
const uint8_t timerArrowCompare = 100; // 2 sec = (61 * 2) + 1
uint8_t timerArrowOnCnt = 0;
const uint8_t timerArrowOnCompare = 10; // 5; // 66 ms = (16.4*(5-1))
uint8_t arrowTakes = 0;
const uint8_t arrowTakesCompare = 1;
uint8_t timerLogCnt = 0;
uint8_t timerLogCompare = 150;
uint8_t timerRadioTimeoutCnt = 0;
const uint8_t timerRadioTimeoutCompare = 80;

uint8_t sendCommandCnt = 10;

#define BUTTON_PIN 8
#define RELAY_PIN 6
#define ARROW_PIN 2

bool buttonPressed = true; // внимание! костыль
bool lightIsOn = true;       // внимание! костыль
bool arrowOn = false;
bool arrowGone = false;
bool sendCommand = false;
uint8_t buttonCount = 0;
uint8_t voltage = 0;
bool logPause = false;

typedef enum {lightNone = 0, lightForceOn, lightForceOff } light_t;
light_t lightStatus = lightNone;

// uint8_t relayState = HIGH; // off
// uint8_t arrowState = LOW; // off

uint32_t wd;
bool reboot = false;

void setup() {
  MCUSR = ~(1 << WDRF); // allow us to disable WD
  wdt_disable();

  wd = 0;

  pinMode(BUTTON_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(ARROW_PIN, OUTPUT);

  digitalWrite(RELAY_PIN, HIGH); // off
  digitalWrite(ARROW_PIN, LOW); // off

  Serial.begin(115200);
  for (byte i = 0; i < 5; i++)
    Serial.println();
  Serial.println("Start");
  printf_begin();
  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio.setDataRate(RF24_250KBPS);
  radio.enableAckPayload();                     // Allow optional ack payloads
  radio.enableDynamicPayloads();                // Ack payloads are dynamic payloads

  if (radioNumber) {
    radio.openWritingPipe(addresses[1]);        // Both radios listen on the same pipes by default, but opposite addresses
    radio.openReadingPipe(1, addresses[0]);     // Open a reading pipe on address 0, pipe 1
  } else {
    radio.openWritingPipe(addresses[0]);
    radio.openReadingPipe(1, addresses[1]);
  }
  radio.startListening();                       // Start listening
  radio.writeAckPayload(1, &counter, 1);        // Pre-load an ack-paylod into the FIFO buffer for pipe 1

  delay(5);
  radio.printDetails();

  rs485.begin();

  //Установка Таймера2.
  TCCR2A = 0;
  TCCR2B = 1 << CS22 | 1 << CS21 | 1 << CS20; // 111 = /1024 = 15625 Hz
  //Подключение прерывания по переполнению Timer2
  TIMSK2 = 1 << TOIE2;  // 15625 / 256 = 61

  wdt_enable(WDTO_1S);
  wdt_reset();
  Serial.println("watchdog 1s");
}

void constructReply() {
  
  reply[0] = 's';
  reply[1] = status + 48; // convert number to ascii digit
  reply[2] = 'c';
  reply[3] = lastcard + 48;
  reply[4] = 'v';

  if (voltage == 0)
    reply[5] = 48; // 0
  else if (voltage > voltageHigh)
    reply[5] = 4 + 48;
  else if (voltage > voltageMid)
    reply[5] = 3 + 48;
  else if (voltage > voltageLow)
    reply[5] = 2 + 48;
  else
    reply[5] = 1 + 48;

//  return reply;
}

void checkCommand() {
  if (compareCmd(rs485data, cmdStatus)) {
    if ((status != linkerr) && (status != cards)  && (status != rferr)) {
      if (voltage == 0)
        status = error;
      else if (voltage > voltageHigh)
        status = vhigh;
      else if (voltage > voltageMid)
        status = vmid;
      else if (voltage > voltageLow)
        status = vlow;
      else
        status = voff;
    }
    //      rs485.sendMsg((const byte*)respStatus[status], 6);
  }
  else if (compareCmd(rs485data, cmdLightOn)) {
    lightStatus = lightForceOn;
    //      rs485.sendMsg((const byte*)respStatus[status], 6);
  }
  else if (compareCmd(rs485data, cmdLightOff)) {
    lightStatus = lightForceOff;
    //      rs485.sendMsg((const byte*)respStatus[status], 6);
  }
  else if (compareCmd(rs485data, cmdLightCancel)) {
    lightStatus = lightNone;
    //      rs485.sendMsg((const byte*)respStatus[status], 6);
  }
  else if (compareCmd(rs485data, cmdArrow)) {
    arrowOn = true;
    status = on;
    digitalWrite(ARROW_PIN, HIGH);
    timerArrowOnCnt = 1;
    //      rs485.sendMsg((const byte*)respStatus[status], 6);
  }
}

void loop() {

//  if (wd < 2000L) {
    if (!reboot)
      wdt_reset();    
//    wd++;
//    Serial.print("loop ");
//    Serial.println(wd);
//  } 
//  else {
//      for (byte i = 0; i < 15; i++)
//    Serial.println("AAAAAAAAAAAAAAAAAAAAAAAAA");
//  }

  switch (lightStatus) {
    case lightForceOn:
      lightOn();
      break;

    case lightForceOff:
      lightOff();
      break;

    case lightNone:
      if (lightIsOn)
        lightOn();
      else
        lightOff();
      break;
  }

  if (rs485.update()) {
    rs485data = rs485.getData();
    #ifdef DEBUG
    Serial.print("Got data: ");
    Serial.write(rs485data, 6);
    Serial.println();
    #endif

    if (compareCmd(rs485data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else if (compareCmd(rs485data, cmdReboot)) {
      reboot = true; // reboot by watchdog
      rs485.sendMsg((const byte*) "rbtset", 6);
    }
    else {
      checkCommand();
      constructReply();
      rs485.sendMsg((const byte*) reply, 6);
    }
    rs485.reset();
  }

  if (sendCommand) {
//    Serial.println("sendFlag");
    radio.stopListening();
    // if (!logPause) {
    //   Serial.print(F("Now sending "));
    // }
    unsigned long time = micros();                          // Record the current microsecond count
    bool ok;
    if (lightIsOn) {
      // if (!logPause) {
      //   Serial.print(" off ");
      // }
      ok = radio.write(&dataOff, len);
//      Serial.println("aft wrOFF");
    }
    else {
      // if (!logPause) {
      //   Serial.print(" on ");
      // }
      ok = radio.write( dataOn, len );
//      Serial.println("aft wrON");
    }
    if (!ok) {
      status = rferr;
      wd++;
      Serial.print("rferror # ");
      Serial.println(wd);
    }
    // if (ok) {
    else {
      status = on;
      if (!radio.available()) {                           // If nothing in the buffer, we got an ack but it is blank
        // if (!logPause) {
        //   Serial.print(F("Got blank response. round-trip delay: "));
        //   Serial.print(micros() - time);
        //   Serial.println(F(" microseconds"));
        // }
      } else {
        byte brtime = 100;
        while (radio.available() ) {                    // If an ack with payload was received
          brtime--;
          if (brtime == 0) break;
          radio.read( data, len );                  // Read it, and display the response time
          unsigned long timer = micros();
          // if (!logPause) {
          //   Serial.print(F("Got response "));
          // }
          bool lon = true;
          bool loff = true;

          byte card = 10;

          for (byte i = 0; i < 5; i++) {
            if (dataEquals(data, dataArrow[i])) {
              card = i;
              timerRadioTimeoutCnt = 0;
              break;
            }
          }

          if (card != 10) {
            Serial.print("card scanned # ");
            Serial.println(card);
            lastcard = card;
            logPause = true;
            timerLogCnt = 0;
            timerRadioTimeoutCnt = 0;
            //                if (status == linkerr)
            //                  status = on;
          }
          if (card == 0) {
            Serial.println("All cards ok! Set arrow");
//            if (!arrowGone)
//              timerArrowCnt = 1; //send arrow
            status = cards;

          }

          byte i = 0;
          while (i < len)
          {
            if (!logPause) {
              Serial.print(data[i], DEC);
              Serial.print(" ");
            }
            if (i == 2) // ignore third byte
              break;    // because it should contain 'voltage'
            if ( data[i] != dataOnResp[i])
              lon = false;
            if ( data[i] != dataOffResp[i])
              loff = false;
            i++;
          }
          if (lon) {
            // if (!logPause) {
            //   Serial.print("FL ON");
            // }
          }
          if (loff) {
            // if (!logPause) {
            //   Serial.print("FL OFF");
            // }
          }
          if (lon or loff) {
            voltage = data[2];
            timerRadioTimeoutCnt = 0;
            if (status == linkerr || status == rferr)
              status = on;
          }

          // if (!logPause) {
          //   Serial.print(F(" round-trip delay: "));
          //   Serial.print(timer - time);
          //   Serial.println(F(" microseconds"));
          // }
        }
      }
    }
    sendCommand = false;
  }

}


void resetGame() {
  Serial.println("resetting all");
  noInterrupts();
  wdt_disable();

  timerArrowCnt = 0;
  timerArrowOnCnt = 0;
  arrowTakes = 0;
  timerLogCnt = 0;
  timerLogCompare = 150;
  timerRadioTimeoutCnt = 0;
  buttonPressed = true; // внимание! костыль
  lightIsOn = true;       // внимание! костыль
  //  lightForceOff = false;
  lightStatus = lightNone;
  arrowOn = false;
  arrowGone = false;
  sendCommand = false;
  buttonCount = 0;
  voltage = 0;
  logPause = false;
  lastcard = 9;

  status = off;
  interrupts();

  Serial.print("Sending radio reset cmd... ");
  radio.stopListening();
  bool ok = radio.write(&dataReset, len);
  if (ok) {
    if (!radio.available()) {                           // If nothing in the buffer, we got an ack but it is blank
      Serial.print(F("Got blank response. round-trip delay: "));
      //      Serial.print(micros()-time);
      Serial.println(F(" microseconds"));
    } else {
      byte brtime = 100;
      while (radio.available() ) {                    // If an ack with payload was received
        brtime--;
        if (brtime == 0) break;
        radio.read( data, len );                  // Read it, and display the response time
        unsigned long timer = micros();
        Serial.print(F("Got response: "));
        for (byte i = 0; i < 3; i++) {
          if (data[i] != dataResetResp[i]) {
            Serial.print("Err!");
            break;
          }
          if (i == 2)
            Serial.print("Success!");
        }
      }
    }
  } else {
    Serial.print("Sending error!");
  }

  wdt_enable(WDTO_1S);
  Serial.println(" done.");
}

//Timer2 overflow interrupt vector handler
ISR(TIMER2_OVF_vect) {

  if (sendCommandCnt <= 0) {
    sendCommand = true; 
    sendCommandCnt = 10;
  }
  else
    sendCommandCnt--;

  // button
  uint8_t btn = digitalRead(BUTTON_PIN);
  if (!buttonPressed && (btn == 0))
    buttonCount++;
  if (buttonPressed && (btn == 1))
    buttonCount++;
  if (buttonPressed && (btn == 0))
    buttonCount = 0;
  if (!buttonPressed && (btn == 1))
    buttonCount = 0;

  if (buttonCount >= 5) {
    if (buttonPressed) {
      lightIsOn = false;
      // digitalWrite(RELAY_PIN, 0);
      buttonPressed = false;
    }
    else {
      lightIsOn = true;
      // digitalWrite(RELAY_PIN, 1);
      buttonPressed = true;
    }
    buttonCount = 0;
  }

  // radio timeout delay
  timerRadioTimeoutCnt++;
  if (timerRadioTimeoutCnt >= timerRadioTimeoutCompare) {
    Serial.println("Radio Timeout!");
    status = linkerr;
    timerRadioTimeoutCnt = 0; // turn off counter
  }


  // arrow delay
  if (timerArrowCnt > 0) {
    timerArrowCnt++;
    if (timerArrowCnt >= timerArrowCompare) {
      arrowOn = true;
      //      Serial.println("Arrow on");
      digitalWrite(ARROW_PIN, HIGH);// TODO: remove digitalWrite from interrupt
      timerArrowOnCnt = 1; // turn on another counter
      timerArrowCnt = 0; // turn off counter
    }
  }

  // arrow ON delay
  if (timerArrowOnCnt > 0) {
    timerArrowOnCnt++;
    if (timerArrowOnCnt >= timerArrowOnCompare) {
      if (arrowOn) {
        arrowOn = false;
        //      arrowGone = true;
        //        Serial.println("Arrow off");
        digitalWrite(ARROW_PIN, LOW);
        arrowTakes++;
        if (arrowTakes >= arrowTakesCompare) {
          timerArrowOnCnt = 0; // turn off counter
          arrowGone = true;
          arrowTakes = 0;
        }
        else {
          timerArrowOnCnt = 1;
        }
      }
      else {
        arrowOn = true;
        digitalWrite(ARROW_PIN, HIGH);
        timerArrowOnCnt = 1;
      }
    }
  }

  // log delay
  if (logPause) {
    timerLogCnt++;
    if (timerLogCnt >= timerLogCompare) {
      logPause = false;
      timerArrowOnCnt = 0; // turn off counter
    }
  }
}

bool dataEquals(byte a[], byte b[]) {
  for (uint8_t k = 0; k < 3; k++) {   // Loop 3 times
    if (a[k] != b[k])     // IF a != b then set match = false, one fails, all fail
      return false;
  }
  return true;      // Return true
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}

inline void lightOff() {
  digitalWrite(RELAY_PIN, LOW);
}

inline void lightOn() {
  digitalWrite(RELAY_PIN, HIGH);
}
