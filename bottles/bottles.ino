
/*

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <EEPROM.h>     // We are going to read and write PICC's UIDs from/to EEPROM
#include <SPI.h>        // RC522 Module uses SPI protocol
#include <MFRC522.h>  // Library for Mifare RC522 Devices

#define SS_1 10
#define RS_1 9
#define SS_2 8
#define RS_2 7
#define SS_3 6
#define RS_3 5
#define SS_4 4
#define RS_4 3
#define SS_5 2
#define RS_5 A0

#define LED_PIN A5
#define LOCK_PIN A4

//const uint8_t RFID_SUCCESS_PIN = A2;
const uint8_t RFID_WIPE_PIN = A1;

bool equalsMaster = false;

uint8_t successRead = 0;    // Variable integer to keep if we have Successful Read from Reader
uint8_t mode = 0;
uint64_t lastTimeMs = 0;
uint64_t wipeTime = 10000;

byte storedCard[4];   // Stores an ID read from EEPROM
byte readCard[4];   // Stores scanned ID read from RFID Module
byte firstMasterCard[4];   // Stores master card's ID read from EEPROM
byte secondMasterCard[4];   // Stores master card's ID read from EEPROM
// byte gameCard[4];   // Stores game card's ID read from EEPROM


byte gameFlags = 0;
byte gameComp = 0;

bool unlocked = false;

// Create MFRC522 instance.

//MFRC522 mfrc522(SS_PIN, RST_PIN);

MFRC522 reader1(SS_1, RS_1);
MFRC522 reader2(SS_2, RS_2);
MFRC522 reader3(SS_3, RS_3);
MFRC522 reader4(SS_4, RS_4);
MFRC522 reader5(SS_5, RS_5);

MFRC522 mfrc522 = reader1;

MFRC522 readers[] = {
  reader1,
  reader2,
  reader3,
  reader4,
  reader5
};

#define READERS_CNT 5

byte gameCards[READERS_CNT][4];

void(*resetFunc)(void) = 0;

void setup() {
  //Arduino Pin Configuration
  pinMode(LOCK_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(RFID_WIPE_PIN, INPUT_PULLUP);
//  digitalWrite(RFID_SUCCESS_PIN, HIGH);

  digitalWrite(LED_PIN, LOW);
  delay(500);
  digitalWrite(LED_PIN, HIGH);
  digitalWrite(LOCK_PIN, LOW);

  //Protocol Configuration
  Serial.begin(115200);  // Initialize serial communications with PC
  SPI.begin();           // MFRC522 Hardware uses SPI protocol
//  mfrc522.PCD_Init();    // Initialize MFRC522 Hardware

  //If you set Antenna Gain to Max it will increase reading distance
  //mfrc522.PCD_SetAntennaGain(mfrc522.RxGain_max);

  Serial.println(F("RFID MFRC522 MODULE V1.0"));   // For debugging purposes
//  ShowReaderDetails(0);  // Show details of PCD - MFRC522 Card Reader details

  for ( uint8_t i = 0; i < READERS_CNT; i++ ) { 
    readers[i].PCD_Init();
    readers[i].PCD_SetAntennaGain(readers[i].RxGain_43dB);
    Serial.print("reader ");
    Serial.println(i);
    ShowReaderDetails(i);

    //init gameComp value
    gameComp |= (1 << i);
  }

  Serial.println(gameComp, BIN);
  // Check if master card defined, if not let user choose a master card
  // This also useful to just redefine the Master Card
  // You can keep other EEPROM records just write other than 127 to EEPROM address 1
  // EEPROM address 1 should hold magical number which is '127'
  if (EEPROM.read(1) != 127) {
    Serial.println(F("Define Cards"));

    Serial.println(F("Scan A PICC to Define as First Master Card"));
    scanFirstMasterCard();

    Serial.println(F("Scan A PICC to Define as Second Master Card"));
    scanSecondMasterCard();

    Serial.println(F("Scan a PICC to ADD to EEPROM"));
    scanGameCards();

    EEPROM.write(1, 127);                  // Write to EEPROM we defined Master Cards.
  }

  Serial.println(F("-------------------"));
  Serial.println(F("First Master Card's UID"));

  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    Serial.print(firstMasterCard[i] = EEPROM.read(2 + i), HEX);
  }
  Serial.println("");

  Serial.println(F("Second Master Card's UID"));
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    Serial.print(secondMasterCard[i] = EEPROM.read(6 + i), HEX);
  }
  Serial.println("");

  Serial.println(F("Game Card's UID"));
  for (uint8_t j = 0; j < READERS_CNT; j++) {
    for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
      Serial.print(gameCards[j][i] = EEPROM.read(10 + i + j*4), HEX);
      Serial.print(" ");
    }
    Serial.println(" ");
  }
  Serial.println("");
  Serial.println(F("-------------------"));
  Serial.println(F("Everything is ready"));
  Serial.println(F("Waiting PICCs to be scanned"));
}

void loop () {
  if (gameFlags == gameComp) {
    digitalWrite(LED_PIN, LOW);
    digitalWrite(LOCK_PIN, HIGH);
    if (!unlocked)
      Serial.println("Unlocked!");
    unlocked = true;
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    digitalWrite(LOCK_PIN, LOW);
    unlocked = false;
  }
  
  processWipeButton();
  for (uint8_t i = 0; i < READERS_CNT; i++) {
//    if (!getID(i))
//      continue;
    readers[i].PCD_Init();
    
    getID(i);
    if ( cardEquals(readCard, gameCards[i])) {
      gameFlags |= (1 << i);
      Serial.print(i);
      Serial.print(gameFlags, BIN);
      Serial.println(F(" Game Card Scanned"));
//      while (mfrc522.PICC_IsOldCardPresent()) {
//        mfrc522.PICC_HaltA();
//        digitalWrite(RFID_SUCCESS_PIN, LOW);
//        delay_ms(500);
//      }
//    Serial.println(F("Game Card Removed"));
    } else {
      gameFlags &= ~(1 << i);  
    }
    //add PICC to EEPROM
    if (cardEquals(readCard, firstMasterCard) || cardEquals(readCard, secondMasterCard)) {
      Serial.println(F("Master Card Scanned"));
      Serial.println(F("Scan a PICC to ADD to EEPROM"));
      scanGameCards();
    }    
    for ( uint8_t i = 0; i < 4; i++) {  //
      readCard[i] = 0;
    }  
  }

  
  
//  delay_ms(500);
}

///////////////////////////////////////// Get PICC's UID ///////////////////////////////////
uint8_t getID(uint8_t num) {
  // Getting ready for Reading PICCs
//  if (!readers[num].PICC_IsNewCardPresent() and !readers[num].PICC_IsOldCardPresent()) { //If a new PICC placed to RFID reader continue
//    return 0;
//  }
//  if (!readers[num].PICC_IsNewCardPresent()) {
//    return 0;
//  }
  readers[num].PICC_IsNewCardPresent();
  if (!readers[num].PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.println(F("Scanned PICC's UID:"));
  for ( uint8_t i = 0; i < 4; i++) {  //
    readCard[i] = readers[num].uid.uidByte[i];
    Serial.print(readCard[i], HEX);
  }
  Serial.println("");
  readers[num].PICC_HaltA(); // Stop reading
  return 1;
}

void ShowReaderDetails(uint8_t num) {
  // Get the MFRC522 software version
  byte v = readers[num].PCD_ReadRegister(readers[num].VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown),probably a chinese clone?"));
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
    Serial.println(F("SYSTEM HALTED: Check connections."));
    while (true); // do not go further
  }
}

//////////////////////////////////////// Read an ID from EEPROM //////////////////////////////
void readID( uint8_t number ) {
  uint8_t start = (number * 4 ) + 2;    // Figure out starting position
  for ( uint8_t i = 0; i < 4; i++ ) {     // Loop 4 times to get the 4 Bytes
    storedCard[i] = EEPROM.read(start + i);   // Assign values read from EEPROM to array
  }
}

///////////////////////////////////////// Add ID to EEPROM   ///////////////////////////////////
void writeID(byte a[], uint8_t start) {
  for ( uint8_t j = 0; j < 4; j++ ) {   // Loop 4 times
    EEPROM.write( start + j, a[j] );  // Write the array values to EEPROM in the right position
  }
}

///////////////////////////////////////// Equal two cards   ///////////////////////////////////
bool cardEquals(byte a[], byte b[]) {
  for (uint8_t k = 0; k < 4; k++) {   // Loop 4 times
    if (a[k] != b[k])     // IF a != b then set match = false, one fails, all fail
      return false;
  }
  return true;      // Return true
}

bool processWipeButton() {
  if (!digitalRead(RFID_WIPE_PIN)) {
    if (lastTimeMs + wipeTime < millis())
    {
      wipeEEPROM();
    }
  } else {
    lastTimeMs = millis();
  }
}

void wipeEEPROM() {
  Serial.println(F("Starting Wiping EEPROM"));
  for (uint16_t x = 0; x < EEPROM.length(); x = x + 1) {    //Loop end of EEPROM address
    if (EEPROM.read(x) == 0) {              //If EEPROM address 0
      // do nothing, already clear, go to the next address in order to save time and reduce writes to EEPROM
    }
    else {
      EEPROM.write(x, 0);       // if not write 0 to clear, it takes 3.3mS
    }
  }
  Serial.println(F("EEPROM Successfully Wiped"));
  resetFunc();
}

void scanFirstMasterCard() {
  do {
    successRead = getID(0);            // sets successRead to 1 when we get read from reader otherwise 0
  } while (!successRead);                  // Program will not go further while you not get a successful read

  writeID(readCard, 2);
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    firstMasterCard[i] = EEPROM.read(2 + i);    // Write it to masterCard
  }
  Serial.println(F("First Master Card Defined"));
}

void scanSecondMasterCard() {
  equalsMaster = false;
  do {
    do {
      successRead = getID(0);            // sets successRead to 1 when we get read from reader otherwise 0
    } while (!successRead);                  // Program will not go further while you not get a successful read

    equalsMaster = cardEquals(firstMasterCard, readCard);
    if (!equalsMaster) {
      writeID(readCard, 6);
      for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
        secondMasterCard[i] = EEPROM.read(6 + i);    // Write it to masterCard
      }
      Serial.println(F("Second Master Card Defined"));
    } else {
      Serial.println(F("It is a first Master Card"));
      Serial.println(F("Scan another PICC to Define as Second Master Card"));
    }
  } while (equalsMaster);
}

void scanGameCards() {
  for (uint8_t i = 0; i < READERS_CNT; i++) {
    equalsMaster = false;
    do {
      do {
        successRead = getID(i);  // sets successRead to 1 when we get read from reader otherwise 0
      } while (!successRead);   //the program will not go further while you are not getting a successful read
  
      if (cardEquals(readCard, firstMasterCard)) {
        Serial.println(F("It is a first Master Card"));
        Serial.println(F("Scan another PICC to Define as Game Card"));
        equalsMaster = true;
      } else if (cardEquals(readCard, secondMasterCard)) {
        Serial.println(F("It is a second Master Card"));
        Serial.println(F("Scan another PICC to Define as Game Card"));
        equalsMaster = true;
      } else {
        writeID(readCard, 10 + i*4);
        for ( uint8_t j = 0; j < 4; j++ ) {          // Read Master Card's UID from EEPROM
          gameCards[i][j] = EEPROM.read(10 + i + j);    // Write it to masterCard
        }
        Serial.println(F("Game Card Defined"));
        equalsMaster = false;
      }
    } while (equalsMaster);
  }
}

void delay_ms(unsigned long ms) {
  uint16_t start = static_cast<uint16_t>(micros());

  while (ms > 0) {
    if ((static_cast<uint16_t>(micros()) - start) >= 1000) {
      ms--;
      start += 1000;
    }
  }
}
