#include <Adafruit_NeoPixel.h>
#include <RS485_q.h>

#define RX_PIN 5
#define TX_PIN 3
#define EN_PIN 4

byte * rs485data;
const byte deviceName[] = "horn..";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const uint8_t LED_PIN = 8; // neopixel output
const uint8_t LOCK_PIN = 7;
////////////////////////////////////////////////
const uint8_t SENS0_PIN = A0;
const uint8_t SENS1_PIN = A1;
const uint8_t SENS2_PIN = A2;
const uint8_t SENS3_PIN = A3;
const uint8_t SENS[4] = { A0, A1, A2, A3};
////////////////////////////////////////////////

const byte cmdStatus[] = "status";
const byte cmdLock[] =     "lock..";
const byte cmdUnlock[] =    "unlock";
const byte cmdReset[] =  "reset.";
const byte cmdReboot[] = "reboot";
byte reply[6];

////////////////////////////////////////////////
uint8_t timerSensorDelayCnt = 0;
const uint8_t timerSensorDelayCompare = 61;
bool sensorFlag = false;

int8_t sensTrig[4] = {0, 0, 0, 0};
uint16_t m_configSensorSensitivityHigh = 700;
uint16_t m_configSensorSensitivityLow = 350;

uint8_t lamp1 = 0;
uint8_t lamp2 = 0;
uint8_t lamp3 = 0;
bool locked = true;

Adafruit_NeoPixel pixel = Adafruit_NeoPixel(1, LED_PIN, NEO_GRB + NEO_KHZ800);

void inline showPixel(uint8_t p1, uint8_t p2, uint8_t p3) {
  pixel.setPixelColor(0, pixel.Color(p1, p2, p3));
  pixel.show();
}

////////////////////////////////////////////////
void setup() {
  pixel.begin();
  pixel.clear();
  pixel.show(); // This sends the updated pixel color to the hardware.
  //Protocol Configuration
  Serial.begin(115200);  // Initialize serial communications with PC
  Serial.println("Start");

  pinMode(LOCK_PIN, OUTPUT);
  lockOn();
  showPixel(255, 0, 0);
  delay(200);
  showPixel(0, 255, 0);
  delay(200);
  showPixel(0, 0, 255);
  delay(200);
  pixel.clear();
  pixel.show();

  rs485.begin();

  TCCR2A = 0;
  TCCR2B = 1 << CS22 | 1 << CS21 | 1 << CS20; // 111 = /1024 = 15625 Hz
  //Подключение прерывания по переполнению Timer2
  TIMSK2 = 1 << TOIE2;  // 15625 / 256 = 61

}

void loop () {

  if (sensorFlag) {
    checkSens();
    sensorFlag = false;
//    rs485.sendMsg((const byte*) "ololol", 6);
  }

//  rs485.sendMsg((const byte*) "ololol", 6);
  delay(100);

  if (rs485.update()) {
    rs485data = rs485.getData();
    #ifdef DEBUG
    Serial.print("Got data: ");
    Serial.write(rs485data, 6);
    Serial.println();
    #endif
    if (compareCmd(rs485data, cmdLock)) {
      lockOn();
    }
    else if (compareCmd(rs485data, cmdUnlock)) {
      lockOff();
    }    

    if (compareCmd(rs485data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else {
      constructReply();
      rs485.sendMsg((const byte*) reply, 6);
    }
    rs485.reset();
  }  

}

//Timer2 overflow interrupt vector handler
ISR(TIMER2_OVF_vect) {
  timerSensorDelayCnt++;
  if (timerSensorDelayCnt >= timerSensorDelayCompare) {
    timerSensorDelayCnt = 0;
    sensorFlag = true;
  }
}

void constructReply() {
  reply[0] = 'l';
  reply[1] = ((locked) ? '1' : '0');
  reply[2] = 'L';
  reply[3] = ((lamp1 == 255) ? '1' : '0');
  reply[4] = ((lamp2 == 255) ? '1' : '0');
  reply[5] = ((lamp3 == 255) ? '1' : '0');
}


void resetGame() {
  lockOn();
  lamp1 = 0;
  lamp2 = 0;
  lamp3 = 0;
}

inline void lockOn() {
  digitalWrite(LOCK_PIN, HIGH); 
  locked = true;
}

inline void lockOff() {
  digitalWrite(LOCK_PIN, LOW); 
  locked = false;
}

void checkSens() {
  for (auto i = 0; i < 4; i++) {
    if (analogRead(SENS[i]) < m_configSensorSensitivityLow)
    {
      sensTrig[i] = -1;
    } else if (analogRead(SENS[i]) > m_configSensorSensitivityHigh)
    {
      sensTrig[i] = 1;
    } else
    {
      sensTrig[i] = 0;
    }
    Serial.print(analogRead(SENS[i])); Serial.println();
  }

  Serial.println(); Serial.println();


  if ((lamp1 == 0) && (sensTrig[0] == -1) && (sensTrig[1] == -1) && (sensTrig[2] == -1) && (sensTrig[3] == -1))
  {
    lamp1 = 255;
    Serial.println("Symbol 1");
  }
  else  if ((lamp2 == 0) && (sensTrig[0] == 1) && (sensTrig[1] == 1) && (sensTrig[2] == 1) && (sensTrig[3] == 1))
  {
    lamp2 = 255;
    Serial.println("Symbol 2");
  } else  if ((lamp3 == 0) && (sensTrig[0] == -1) && (sensTrig[1] == 1) && (sensTrig[2] == -1) && (sensTrig[3] == 1))
  {
    lamp3 = 255;
    Serial.println("Symbol 3");
  }
  //    else
  //    {
  //      pixel.clear(); // Moderately bright green color.
  //      pixel.show();
  //    }

  showPixel(lamp1, lamp2, lamp3);
  if ((lamp1 == 255) && (lamp2 == 255) && (lamp3 == 255)) {
    digitalWrite(LOCK_PIN, LOW);
  }
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}

