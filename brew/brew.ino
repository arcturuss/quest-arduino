#include <SPI.h>        // RC522 Module uses SPI protocol 
#include <MFRC522.h>  // Library for Mifare RC522 Devices
#include <RS485_q.h>
#include <EEPROM.h>

// for SPI
#define SS_PIN A0
#define RS_PIN A1

// for RS485
#define TX_PIN 10 
#define RX_PIN 8
#define EN_PIN 9

#define LAMP_PIN 7
#define AIR_PIN  6
#define PUMP_PIN 5
#define PUMP2_PIN 2

#define BTN1_PIN 3 // pulled down
#define BTN2_PIN 4 // pulled down


byte * rs485data;
const byte deviceName[] = "brew..";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdOn[] =     "turnon";
const byte cmdOff[] =    "trnoff";
const byte cmdStart[] =  "start.";
const byte cmdStop[] =   "stop..";
const byte cmdReset[] =  "reset.";
const byte cmdReboot[] = "reboot";
//byte reply[6];


MFRC522 reader(SS_PIN, RS_PIN);
//byte gameUid[4] = {0x04, 0xA1, 0x26, 0x5A};
byte gameUid[4] = {0x04, 0xCA, 0x26, 0x5A};
byte gameCard[4];
byte storedCard[4];   // Stores an ID read from EEPROM
byte readCard[4];   // Stores scanned ID read from RFID Module
byte firstMasterCard[4];   // Stores master card's ID read from EEPROM

bool timerFlag = false;
uint8_t timerCompare = 15;
uint8_t timerCnt = 0;

bool buttonFlag = false;
uint8_t timerButton = 0;
#define BUTTON_DEBOUNCE 2
uint8_t timerPressed = 0;
#define TIME_PRESSED 61

uint8_t buttonState = 0; // 0 = off, 1 = debounce, 2 = on, 3 = on-debounce
uint8_t pumpState = 0; // 0 = disabled, 1 = waiting, 2 = enabled, 3 = action, 4 = disabled after action, 
                       // 5 = waiting after action, 6 = enabled after action
bool isCardPresent = false;
bool pressed = false;
bool pumpFlagState1 = false;

bool forceOn = false;
bool forcePump = false;

bool reader_ok = false;

uint32_t msPump = 0;
#define PUMP_TIME 9500
#define PUMP_PAUSE 60000


void inline pumpOn() {
  digitalWrite(PUMP_PIN, LOW); // on
  digitalWrite(PUMP2_PIN, HIGH);
}

void inline pumpOff() {
  digitalWrite(PUMP_PIN, HIGH); // off
  digitalWrite(PUMP2_PIN, LOW); 
}


void setup() {
  // put your setup code here, to run once:
  pinMode(LAMP_PIN, OUTPUT);
  pinMode(AIR_PIN, OUTPUT);
  pinMode(PUMP_PIN, OUTPUT);
  pinMode(PUMP2_PIN, OUTPUT);
  digitalWrite(AIR_PIN,  HIGH); // relay, high = off
  digitalWrite(LAMP_PIN, HIGH); // lamp is off by default
  digitalWrite(PUMP_PIN, HIGH);
  digitalWrite(PUMP2_PIN, LOW); // non-relay, low = off

  pinMode(BTN1_PIN, INPUT);
  pinMode(BTN2_PIN, INPUT);

  Serial.begin(115200);

  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV128);
  reader.PCD_Init();  
  byte v = reader.PCD_ReadRegister(reader.VersionReg);
  if ((v != 0x00) && (v != 0xFF))
    reader_ok = true;

  // enable Timer2
  TCCR2A = 0; 
  TCCR2B = 1<<CS22 | 0<<CS21 | 1<<CS20; // 101 = /1024
  TIMSK2 = 1<<TOIE2;  //Подключение прерывания по переполнению Timer2

  rs485.begin();
  rs485.reset();


  if (EEPROM.read(1) != 127) {
    blocking_beep(3, 150);
    Serial.println(F("Define Cards"));

    Serial.println(F("Scan A PICC to Define as First Master Card"));
    scanFirstMasterCard();

    Serial.println(F("Scan a PICC to ADD to EEPROM"));
    scanGameCard();

    EEPROM.write(1, 127);                  // Write to EEPROM we defined Master Cards.

    blocking_beep(1, 500);
  }

  readCardUids();

  Serial.println("Start");
}

void loop() {

  process_rs485();
  
  if (buttonFlag) {
    buttonFlag = false;
//    timerButton = BUTTON_DEBOUNCE;
    buttonState = checkButton(buttonState);
    
     if (buttonState == 0)            // disabled
       digitalWrite(LAMP_PIN, HIGH);
     if (buttonState == 2 || forceOn) 
       digitalWrite(LAMP_PIN, LOW);

    if ((buttonState >= 2) || forceOn) {
      if (!pressed) {
        digitalWrite(AIR_PIN, LOW);
        timerPressed = TIME_PRESSED;
        pressed = true;
      }
      if (pumpFlagState1) {
        pumpFlagState1 = false;
        if (pumpState != 2)
          digitalWrite(AIR_PIN, HIGH);
      }
    } 
    else {
      pressed = false;
      pumpOff();
      digitalWrite(AIR_PIN, HIGH);
      pumpState = 0;
    }
  }
  if (timerFlag) {
    timerFlag = false;
    
    //isCardPresent = getCard();
    isCardPresent = false;
    if (getCard()) {
      if (cardEquals(readCard, gameCard)) {
        isCardPresent = true;
      }
      else if (cardEquals(readCard, firstMasterCard)) {
        blocking_beep(3, 150);
        Serial.println(F("Define new game card!"));
    
        Serial.println(F("Scan a PICC to ADD to EEPROM"));
        scanGameCard();
    
        blocking_beep(1, 500);
      }
    }

    Serial.print("btn ");
    Serial.print(buttonState);
    Serial.print(" card ");
    Serial.print(isCardPresent);
    Serial.print(" pump ");
    Serial.println(pumpState);
    
    switch (pumpState) {
      case 0:
        if ((buttonState >= 2 || forceOn) && (isCardPresent || forcePump)) {
          pumpState = 2;
          msPump = millis();
          pumpOn();
          digitalWrite(AIR_PIN, LOW);
          Serial.println("start pump");
        }
        break;

      case 2:
        if (millis() >= (msPump + PUMP_TIME)) {
          pumpState = 3;
          pumpOff();
          digitalWrite(AIR_PIN, HIGH);
          msPump = millis();
          Serial.println("stop pump");
          forcePump = false; // to avoid pump auto-restart
        }
        if (!isCardPresent && !forcePump) {
          Serial.println("stop - no card");
          pumpOff();
          digitalWrite(AIR_PIN, HIGH);
          if (millis() >= (msPump + PUMP_TIME - 2500)) {
            msPump = millis();
            pumpState = 3;
            Serial.println("pause");
          }
          else{
            pumpState = 0;
            Serial.println("reset");
          }
        }
        break;

      case 3:
        if (millis() >= (msPump + PUMP_PAUSE)) {
          if (!isCardPresent) {
            pumpState = 0;
            Serial.println("unpause");
          }
        }
        break;

    }

  }
  
  

}



void process_rs485() {
  if (rs485.update()) {
    rs485data = rs485.getData();
    Serial.println("rs485!");
//    Serial.println();
    Serial.write(rs485data, 6);
    Serial.println();
    if (compareCmd(rs485data, cmdOn)) {
      forceOn = true;
      forcePump = false; // to avoid accidents when pump is enabled
      Serial.println("FORCE ON");
    }
    else if (compareCmd(rs485data, cmdOff)) {
      forceOn = false;
      forcePump = false;
      pumpOff();
      Serial.println("-- FORCE OFF");
    }    
    else if (compareCmd(rs485data, cmdStop)) {
      pumpOff();
      forcePump = false;
      digitalWrite(AIR_PIN, HIGH);
      pumpState = 3;
    }        
    else if (compareCmd(rs485data, cmdStart)) {
      forceOn = true;
      forcePump = true;
      Serial.println("FORCE ON+START");
    }    
    
    if (compareCmd(rs485data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else {
      if (!reader_ok)
        rs485.sendMsg((const byte*) "reader", 6);
      else if (pumpState == 2)
        rs485.sendMsg((const byte*) "pumpon", 6);
      else if (pumpState == 3)
        rs485.sendMsg((const byte*) "paused", 6);
      else if (buttonState >= 2)
        rs485.sendMsg((const byte*) "ison..", 6);
      else if (forceOn)
        rs485.sendMsg((const byte*) "forcon", 6);
      else
        rs485.sendMsg((const byte*) "isoff.", 6);
    }
    rs485.reset();
  }
}

void resetGame() {
  noInterrupts();
  
  digitalWrite(AIR_PIN,  HIGH); // relay, high = off
  digitalWrite(LAMP_PIN, HIGH); // lamp is off by default
  digitalWrite(PUMP_PIN, HIGH);
  digitalWrite(PUMP2_PIN, LOW); // non-relay, low = off

  reader.PCD_Init();  
  pumpOff();
  pumpState = 0;
  buttonState = 0;

  isCardPresent = false;
  pressed = false;
  pumpFlagState1 = false;

  forceOn = false;
  forcePump = false;  

  interrupts();
}

bool getCard() {
  // read uid to global variable and return
  
  reader.PCD_Init(); //re-init
  byte v = reader.PCD_ReadRegister(reader.VersionReg);
  if ((v != 0x00) && (v != 0xFF))
    reader_ok = true;
  else {
    reader_ok = false;
    return false;
  }
  if (!reader.PICC_IsNewCardPresent()) {
    return false;
  }
  if (!reader.PICC_ReadCardSerial()) {   // Since a PICC placed get Serial and continue
    return false;
  }  
//  byte uid[4];
  for (byte i = 0; i < reader.uid.size; i++) {
    readCard[i] = reader.uid.uidByte[i];
  }
//  byte test = 1;
//  test = memcmp(gameUid, uid, 4);      
  reader.PICC_HaltA(); // Stop reading

  return true;

//  if ( 0 == test ) // got correct card!
//    return true;
//  else
//    return false;
}

uint8_t checkButton(uint8_t b) {
  switch (b) {
    case 0:
      if (digitalRead(BTN1_PIN) == HIGH)
        return 1;
      else
        return 0;

    case 1:
      if (digitalRead(BTN1_PIN) == HIGH)
        return 2;
      else
        return 0;

    case 2:
      if (digitalRead(BTN1_PIN) == LOW)
        return 3;
      else
        return 2;

    case 3:
      if (digitalRead(BTN1_PIN) == LOW)
        return 0;
      else
        return 2;
  }  
}

ISR(TIMER2_OVF_vect) {
  if (timerCnt < timerCompare) {
    timerCnt++;
  } else {
    timerCnt = 0;
    timerFlag = true;
  }

  if (timerButton == 0) {
    buttonFlag = true;
    timerButton = BUTTON_DEBOUNCE;
  }
  else {
    timerButton--;
  }
  if (timerPressed > 0) {
    if (timerPressed == 1) {
      pumpFlagState1 = true;
    }
    timerPressed--;
  }  
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}

void blocking_beep(uint8_t repeats, uint16_t time_ms) {
  for (uint8_t i = 0; i < repeats; i++) {
    digitalWrite(AIR_PIN, LOW);
    delay(time_ms);
    digitalWrite(AIR_PIN, HIGH);
    delay(time_ms);
  }
}

void scanFirstMasterCard() {
  bool successRead = false;
  do {
    successRead = getCard();            // sets successRead to 1 when we get read from reader otherwise 0
  } while (!successRead);                  // Program will not go further while you not get a successful read

  writeID(readCard, 2);
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    firstMasterCard[i] = EEPROM.read(2 + i);    // Write it to masterCard
  }
  
  Serial.println(F("First Master Card Defined"));
  blocking_beep(2, 200);  
}

void scanGameCard() {
  bool successRead = false;
  bool equalsMaster = false;
  do {
    do {
      successRead = getCard();  // sets successRead to 1 when we get read from reader otherwise 0
    } while (!successRead);   //the program will not go further while you are not getting a successful read

    if (cardEquals(readCard, firstMasterCard)) {
      Serial.println(F("It is a first Master Card"));
      Serial.println(F("Scan another PICC to Define as Game Card"));
      blocking_beep(3, 150); 
      equalsMaster = true;
    } else {
      writeID(readCard, 10);
      for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
        gameCard[i] = EEPROM.read(10 + i);    // Write it to masterCard
      }
      blocking_beep(2, 200);  
      equalsMaster = false;
    }
  } while (equalsMaster);
  
}

//////////////////////////////////////// Read an ID from EEPROM //////////////////////////////
void readID( uint8_t number ) {
  uint8_t start = (number * 4 ) + 2;    // Figure out starting position
  for ( uint8_t i = 0; i < 4; i++ ) {     // Loop 4 times to get the 4 Bytes
    storedCard[i] = EEPROM.read(start + i);   // Assign values read from EEPROM to array
  }
}

void readCardUids() {
  for ( uint8_t i = 0; i < 4; i++ ) {
    firstMasterCard[i] = EEPROM.read(2 + i);
  }
  for ( uint8_t i = 0; i < 4; i++ ) {
    gameCard[i] = EEPROM.read(10 + i);
  }
}

///////////////////////////////////////// Add ID to EEPROM   ///////////////////////////////////
void writeID(byte a[], uint8_t start) {
  for ( uint8_t j = 0; j < 4; j++ ) {   // Loop 4 times
    EEPROM.write( start + j, a[j] );  // Write the array values to EEPROM in the right position
  }
}

///////////////////////////////////////// Equal two cards   ///////////////////////////////////
bool cardEquals(byte a[], byte b[]) {
  for (uint8_t k = 0; k < 4; k++) {   // Loop 4 times
    if (a[k] != b[k])     // IF a != b then set match = false, one fails, all fail
      return false;
  }
  return true;      // Return true
}

//bool processWipeButton() {
//  if (!digitalRead(RFID_WIPE_PIN)) {
//    if (lastTimeMs + wipeTime < millis())
//    {
//      wipeEEPROM();
//    }
//  } else {
//    lastTimeMs = millis();
//  }
//}

void wipeEEPROM() {
  Serial.println(F("Starting Wiping EEPROM"));
  for (uint16_t x = 0; x < EEPROM.length(); x = x + 1) {    //Loop end of EEPROM address
    if (EEPROM.read(x) == 0) {              //If EEPROM address 0
      // do nothing, already clear, go to the next address in order to save time and reduce writes to EEPROM
    }
    else {
      EEPROM.write(x, 0);       // if not write 0 to clear, it takes 3.3mS
    }
  }
  Serial.println(F("EEPROM Successfully Wiped"));
//  resetFunc();
}
