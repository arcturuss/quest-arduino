#include <RS485_q.h>
#include <SoftwareSerial.h>
#include <DFMiniMp3.h>

#define RX_PIN 10
#define TX_PIN 12
#define EN_PIN 11

#define LED_PIN 2
#define LIGHT_PIN 4
#define LOCK_PIN 8
#define COIL_PIN 9

#define BUTTON_PIN 5

// #define RS485_DEBUG
#define DEBUG

#define VOICE_VOL  26
#define VOICE2_VOL 26
#define SHOT_VOL   29


class Mp3Notify
{
public:
  static void OnError(uint16_t errorCode)
  {
    // see DfMp3_Error for code meaning
    Serial.println();
    Serial.print("Com Error ");
    Serial.println(errorCode);
  }
  static void OnPlayFinished(uint16_t track)
  {
    Serial.print("Play finished for #");
    Serial.println(track);  
  }
  static void OnCardOnline(uint16_t code)
  {
    Serial.println("Card online ");
  }
  static void OnCardInserted(uint16_t code)
  {
    Serial.println("Card inserted ");
  }
  static void OnCardRemoved(uint16_t code)
  {
    Serial.println("Card removed ");
  }
};

volatile bool updateLed = false;
volatile bool anacompState = false;

byte * rs485_data;
const byte cmdStatus[] = "status";
const byte cmdUnlock[] = "unlock";
const byte cmdLock[]   = "lock..";
const byte cmdReset[]  = "reset.";
const byte cmdReboot[] = "reboot";
const byte cmdShoot[]  = "doshot";
const byte cmdShoot2[] = "shot2.";
const byte cmdVoice1[] = "voice1";
const byte cmdVoice2[] = "voice2";
const byte cmdVoice3[] = "voice3";
const byte cmdStart[] = "start.";
const byte cmdEnd[]   = "end...";
const char* respStatus[] = { "locked", "unlckd", "ison..", "gotsht", "snderr"};

const byte deviceName[] = "shoot.";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

typedef enum { locked = 0, unlocked, on, gotShot, sounderror} status_t;
status_t status = locked;

bool lockClosed = true;

bool shotFlag = false;
uint8_t shotCount = 0;
uint16_t timerShotCnt = 0;
const uint16_t timerShotCompare = 61*5;
byte timerLightCnt = 0;
const byte timerLightCompare = 8;
byte timerButtonCnt = 0;
const byte timerButtonCompare = 3;
bool buttonHandled = false;

SoftwareSerial softSerial(A1, A0); // RX, TX
DFMiniMp3<SoftwareSerial, Mp3Notify> mp3(softSerial);

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(LOCK_PIN, OUTPUT);
  pinMode(COIL_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  digitalWrite(LED_PIN, 0);
  digitalWrite(LIGHT_PIN, 0);
  digitalWrite(LOCK_PIN, 1); // locked
  digitalWrite(COIL_PIN, 0);

  Serial.begin(115200);

  mp3.begin();
//  mp3.reset(); 
//
//  // show some properties and set the volume
//  uint16_t volume = mp3.getVolume();
//  Serial.print("volume ");
//  Serial.println(volume);
  mp3.setVolume(30);
//  
//  uint16_t count = mp3.getTotalTrackCount();
//  Serial.print("files ");
//  Serial.println(count);

//  ACSR  |= _BV(ACIE);                       // Разрешить прерывание аналогового компаратора
//  DIDR1 |= _BV(AIN1D) | _BV(AIN0D);         // Выключить цифровые входы контактов Digital 6 и 7 (для снижения энергопотребления)  

  TCCR2A = 0;
  TCCR2B = 1 << CS22 | 1 << CS21 | 1 << CS20; // 111 = /1024 = 15625 Hz
  //Подключение прерывания по переполнению Timer2
  TIMSK2 = 1 << TOIE2;  // 15625 / 256 = 61

  interrupts(); 
  rs485.begin();
  Serial.println("shooter started");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (updateLed) {                          // Если произошло прерывание аналогового компаратора
    updateLed = false;                      // Отметить, что данное событие обработано
    if (anacompState)                       // И если компаратор в состоянии логической единицы
      digitalWrite(LED_PIN, LOW);  // включить светодиод ANACOMP_LED
    else                                    // В противном случае
      digitalWrite(LED_PIN, HIGH);   // выключить светодиод ANACOMP_LED
      if (status == locked)
        status = gotShot;
  }

  if (shotFlag) {
    shotFlag = false;
    doShot();
    if (status == on) {
      if (shotCount == 0) {
        shotCount = 2 + random(3);
        timerShotCnt = 70 + 29*(random(7));
      }
      else {
        shotCount--;
        timerShotCnt = 240 + 5*(random(8));
      }
    }
  }

  if (rs485.update()) {
    rs485_data = rs485.getData();
    #ifdef DEBUG
    Serial.print("Got data: ");
    Serial.write(rs485_data, 6);
    Serial.println();
    #endif
    if (compareCmd(rs485_data, cmdStatus)) {
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }
    else if (compareCmd(rs485_data, cmdUnlock)) {
      lockOpen();
      if (status != on)
        status = unlocked;
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }
    else if (compareCmd(rs485_data, cmdLock)) {
      lockClose();
      if (status != on)
        status = locked;
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }
    else if (compareCmd(rs485_data, cmdStart)) {
      // status = on;
      rs485.sendMsg((const byte*)respStatus[status], 6);
      startShooting();
    }    
    else if (compareCmd(rs485_data, cmdEnd)) {
      if (lockClosed)
        status = locked;
      else
        status = unlocked;
      timerShotCnt = 0;
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }

    else if (compareCmd(rs485_data, cmdShoot)) {
      // status = on;
      rs485.sendMsg((const byte*)respStatus[status], 6);
      doShot();
    }
    else if (compareCmd(rs485_data, cmdShoot2)) {
      // status = on;
      rs485.sendMsg((const byte*)respStatus[status], 6);
      doShot2();
    }    
    else if (compareCmd(rs485_data, cmdVoice1)) {
      mp3.setVolume(VOICE_VOL);
      mp3.playFolderTrack(2, 1);
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }    
    else if (compareCmd(rs485_data, cmdVoice2)) {
      mp3.setVolume(VOICE2_VOL);
      mp3.playFolderTrack(2, 2);
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }    
    else if (compareCmd(rs485_data, cmdVoice3)) {
      mp3.setVolume(VOICE_VOL);
      mp3.playFolderTrack(2, 3);
      rs485.sendMsg((const byte*)respStatus[status], 6);
    }    
    else if (compareCmd(rs485_data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    rs485.reset();
  }

  mp3.loop();
}

ISR(ANALOG_COMP_vect)
{
  anacompState = ACSR & _BV(ACO);           // Запомнить состояние аналогового компаратора
  updateLed    = true;                      // Обновить состояние светодиода ANACOMP_LED
}

ISR(TIMER2_OVF_vect) {
//  if (digitalRead(BUTTON_PIN) == LOW) {
//    if ((timerButtonCnt >= timerButtonCompare) && !buttonHandled) {
//      doShot();
//      buttonHandled = true;
//      timerButtonCnt = 0;
//    }
//    else {
//      timerButtonCnt++;
//    }
//  }
//  else {
//    timerButtonCnt = 0;
//    buttonHandled = false;
//  }

  if (timerShotCnt > 0) {
    if (timerShotCnt >= timerShotCompare) {
      timerShotCnt = 0;
      shotFlag = true;
    }
    else {
      timerShotCnt++;
    }
  }

  if (timerLightCnt > 0) {
    if (timerLightCnt >= timerLightCompare) {
      timerLightCnt = 0;
      digitalWrite(LIGHT_PIN, 0);
      digitalWrite(COIL_PIN, 0);
      if (lockClosed) {
        status = locked;
      }
      else {
        status = unlocked;
      }
//      ACSR  |= _BV(ACIE);  // enablr analog_comp ISR
    }
    else {
      timerLightCnt++;
    }
  }

}

inline void startShooting() {
  status = on;
  doShot(); // delay(105) inside
  shotCount = 2 + random(3);
  timerShotCnt = 240 + 5*(random(8));
  // timerShotCnt = 1 + 61*(random(6)); // delay from 1 to 6 sec
}

inline void doShot() {
  mp3.setVolume(SHOT_VOL);
//  ACSR  &= ~_BV(ACIE);  // disable analog_comp ISR
  mp3.playFolderTrack(1, random(1, 4));  // sd:/01/001.mp3 up to 003.mp3
  delay(20);
  digitalWrite(LIGHT_PIN, 1);
  // delayMicroseconds(100);
  // digitalWrite(COIL_PIN, 1);
  delay(100);
  digitalWrite(LIGHT_PIN, 0);
  // digitalWrite(COIL_PIN, 0);
//  timerLightCnt = 1;
//  if (lockClosed) {
//    status = locked;
//  }
//  else {
//    status = unlocked;
//  }
}

inline void doShot2() {
  mp3.setVolume(SHOT_VOL);
  mp3.playFolderTrack(1, 4);  // sd:/01/004.mp3
  delay(5);
  digitalWrite(LIGHT_PIN, 1);
  delay(50);
  digitalWrite(COIL_PIN, 1);
  delay(50);
  digitalWrite(LIGHT_PIN, 0);
  delay(20);
  digitalWrite(COIL_PIN, 0);
//  if (lockClosed) {
//    status = locked;
//  }
//  else {
//    status = unlocked;
//  }
}

inline void lockOpen() {
  digitalWrite(LOCK_PIN, 0);
  lockClosed = false;
}

inline void lockClose() {
  digitalWrite(LOCK_PIN, 1);
  lockClosed = true;
}

void resetGame() {
  // noInterrupts();

  digitalWrite(LED_PIN, 0);
  digitalWrite(LIGHT_PIN, 0);
  lockClose();
  digitalWrite(COIL_PIN, 0);

  mp3.stop();
  // mp3.reset();

  timerLightCnt = 0;
  status = locked;
  updateLed = false;

  // ACSR  |= _BV(ACIE);                       // Разрешить прерывание аналогового компаратора
  // DIDR1 |= _BV(AIN1D) | _BV(AIN0D);         // Выключить цифровые входы контактов Digital 6 и 7 (для снижения энергопотребления)  

  // TCCR2A = 0;
  // TCCR2B = 1 << CS22 | 1 << CS21 | 1 << CS20; // 111 = /1024 = 15625 Hz
  //Подключение прерывания по переполнению Timer2
  // TIMSK2 = 1 << TOIE2;  // 15625 / 256 = 61

  // interrupts();
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}
