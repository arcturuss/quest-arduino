#include <EEPROM.h>
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

#include <MFRC522.h>

RF24 radio(7,8);
bool radioNumber = 0; // 0 = flashlight, 1 = arrow
//const uint64_t pipe = 0xE8E8F0F0E1LL; // Single radio pipe address for the 2 nodes to communicate.
byte addresses[][6] = {"1N1de","2N2de"}; // from example
byte counter = 1;     // A single byte to keep track of the data being sent back and forth

const uint8_t len = 3;
uint8_t data[len];
const uint8_t data_on[] = { 22, 0, 23 };
const uint8_t data_off[] = { 0, 25, 0 };
const uint8_t data_reset[] = { 42, 24, 42 };
uint8_t data_on_resp[len] = { 14, 48, 0 };
uint8_t data_off_resp[len] = { 47, 13, 0 };
uint8_t data_reset_resp[len] = { 54, 18, 54 };

uint8_t data_arrow[][3] = {{ 33, 11, 25 },  // ok
                            {33, 12, 26 },  // H
                            {33, 13, 27 },  // E
                            {33, 14, 28 },  // L
                            {33, 15, 29 }}; // P
bool on = false;
bool off = false;
//uint8_t gotCard = 0;
uint8_t voltage = 0; // accumulator voltage

bool timerFlag = false;


MFRC522 reader(10, 9);

bool equalsMaster = false;

uint8_t successRead = 0;    // Variable integer to keep if we have Successful Read from Reader
uint8_t mode = 0;
//uint64_t lastTimeMs = 0;
//uint64_t wipeTime = 10000;

byte storedCard[4];   // Stores an ID read from EEPROM
byte readCard[4];   // Stores scanned ID read from RFID Module
byte firstMasterCard[4];   // Stores master card's ID read from EEPROM
byte secondMasterCard[4];   // Stores master card's ID read from EEPROM
byte gameCards[4][4];   // Stores game card's ID read from EEPROM

byte currCard = 0; 
byte sendCard = 0;
byte prevCard = 0;
byte nextCard = 0;

#define LIGHT_PIN 3
#define LIGHT2_PIN 2
#define VOLTAGE_PIN A7

void setup() {
  // put your setup code here, to run once:
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(LIGHT2_PIN, OUTPUT);
  digitalWrite(LIGHT_PIN, 0);
  digitalWrite(LIGHT2_PIN, 1);  

  //blink
  delay(500);
  digitalWrite(LIGHT_PIN, 1);
  digitalWrite(LIGHT2_PIN, 0);
  delay(500);
  digitalWrite(LIGHT_PIN, 0);
  digitalWrite(LIGHT2_PIN, 1);
  delay(500);
  digitalWrite(LIGHT_PIN, 1);
  digitalWrite(LIGHT2_PIN, 0);
  delay(500);
  digitalWrite(LIGHT_PIN, 0);
  digitalWrite(LIGHT2_PIN, 1);
  delay(500);
  digitalWrite(LIGHT_PIN, 1);
  digitalWrite(LIGHT2_PIN, 0);
  delay(500);
  digitalWrite(LIGHT_PIN, 0);
  digitalWrite(LIGHT2_PIN, 1);

  Serial.begin(115200);
  printf_begin();

  SPI.begin();
  reader.PCD_Init();
  reader.PCD_SetAntennaGain(reader.RxGain_43dB);  
  Serial.println("RC522");
  ShowReaderDetails();  

  if (EEPROM.read(1) != 127) {
    Serial.println(F("Define Cards"));

    Serial.println(F("Scan A PICC to Define as First Master Card"));
    scanFirstMasterCard();

//    Serial.println(F("Scan A PICC to Define as Second Master Card"));
//    scanSecondMasterCard();

    Serial.println(F("Scan a PICC to ADD to EEPROM"));
    scanGameCards();

    EEPROM.write(1, 127);                  // Write to EEPROM we defined Master Cards.
  }
  
  Serial.println(F("-------------------"));
  Serial.println(F("First Master Card's UID"));

  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    Serial.print(firstMasterCard[i] = EEPROM.read(2 + i), HEX);
  }
  Serial.println("");

//  Serial.println(F("Second Master Card's UID"));
//  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
//    Serial.print(secondMasterCard[i] = EEPROM.read(6 + i), HEX);
//  }
//  Serial.println("");

  Serial.println(F("Game Card's UID"));
  for (uint8_t j = 0; j < 4; j++) {
    for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
      Serial.print(gameCards[j][i] = EEPROM.read(10 + i + j*4), HEX);
      Serial.print(" ");
    }
    Serial.println(" ");
  }
  Serial.println("");
  Serial.println(F("-------------------"));
  Serial.println(F("Everything is ready"));
  Serial.println(F("Waiting PICCs to be scanned"));


  delay(5);
  
  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio.setDataRate(RF24_250KBPS);

  radio.enableAckPayload();                     // Allow optional ack payloads
  radio.enableDynamicPayloads();                // Ack payloads are dynamic payloads
  
  if(radioNumber){
    radio.openWritingPipe(addresses[1]);        // Both radios listen on the same pipes by default, but opposite addresses
    radio.openReadingPipe(1,addresses[0]);      // Open a reading pipe on address 0, pipe 1
  }else{
    radio.openWritingPipe(addresses[0]);
    radio.openReadingPipe(1,addresses[1]);
  }
  radio.startListening();                       // Start listening  
  
  radio.writeAckPayload(1,&counter,1);          // Pre-load an ack-paylod into the FIFO buffer for pipe 1
  
//  radio.openReadingPipe(1,pipe);
//  radio.startListening();
  delay(5);
  Serial.println("NRF24L01+");
  radio.printDetails();

  delay(2000);
  
  Serial.println("Start");

  //Установка Таймера2.
  TCCR2A = 0;
  TCCR2B = 1 << CS22 | 0 << CS21 | 1 << CS20; // 101 = /1024 = 15625 Hz
  //Подключение прерывания по переполнению Timer2
  TIMSK2 = 1 << TOIE2;  // 15625 / 256 = 61  
}

void loop() {

    byte pipeNo;                          // Declare variables for the pipe and the byte received
    while( radio.available(&pipeNo)){     // Read all available payloads
//      radio.read( &gotByte, 1 );     
      radio.read( data, len );              

      // check package
      on = true;
      off = true;
      bool reset = true;
      int i = len;
      while(i--)
      {
        Serial.print(data[i], HEX);
        if ( data[i] != data_on[i])
          on = false;
        if ( data[i] != data_off[i])
          off = false;
        if ( data[i] != data_reset[i])
          reset = false;
      }
      Serial.println();    
      if (sendCard > 0) {
        if (sendCard < 5)
          radio.writeAckPayload(pipeNo,&data_arrow[sendCard], len );
        if (sendCard == 5)
          radio.writeAckPayload(pipeNo,&data_arrow[0], len );
        sendCard = 0;
      }
      else {
        if (off) {
          radio.writeAckPayload(pipeNo,&data_off_resp, len );
        }
        if (on) {
          radio.writeAckPayload(pipeNo,&data_on_resp, len );
        }
        if (reset) {
          radio.writeAckPayload(pipeNo,&data_reset_resp, len );
        }
      }
      if (off) {
        digitalWrite(LIGHT_PIN, 1);
        digitalWrite(LIGHT2_PIN, 0);
      }
      if (on) {
        digitalWrite(LIGHT_PIN, 0);
        digitalWrite(LIGHT2_PIN, 1);
      }      
      if (reset) {
        resetGame();
      }
      off = false;
      on = false;   
      reset = false; 

                                                     // Since this is a call-response. Respond directly with an ack payload.
//      gotByte += 1;                                // Ack payloads are much more efficient than switching to transmit mode 
                                                     // to respond to a call
//      radio.writeAckPayload(pipeNo,&data_on_resp, len );  // This can be commented out to send empty payloads.
//      Serial.print(F("Loaded next response "));
//      Serial.println(gotByte);  
   }
   
  if (timerFlag) {
    timerFlag = false;
    voltage = analogRead(VOLTAGE_PIN) >> 2; // to 8-bit
    data_on_resp[2]  = voltage;
    data_off_resp[2] = voltage;


    // start again
    if (nextCard == 4) {
      sendCard = 5;
      nextCard = 0;
    }    
  
    //MFRC522
    if (getID()) {

      if (cardEquals(readCard, firstMasterCard)) {
        Serial.println(F("Master Card Scanned"));
        Serial.println(F("Scan a PICC to ADD to EEPROM"));
        scanGameCards();
      }
  
      // currCard = 10;
      for ( uint8_t i = 0; i < 4; i++) {  //
        if (cardEquals(readCard, gameCards[i])) {
          prevCard = currCard;
          currCard = i;
          Serial.print(currCard + 1);
          Serial.println(F(" Game Card Scanned"));  
          break;      
        }
      }  
      Serial.print("prevCard ");
      Serial.println(prevCard);
      Serial.print("nextCard ");
      Serial.println(nextCard);      
      if (currCard == nextCard) {
        nextCard++;
        sendCard = nextCard;
        Serial.println(">>");
      }
      else {
        if ((currCard != prevCard)) {
          nextCard = 0;
          Serial.println("Wrong card");
        }
      }

        
    }
    
  }

}

//Timer2 overflow interrupt vector handler
ISR(TIMER2_OVF_vect) {
  timerFlag = true; 
}

void resetGame() {
  voltage = 0; // accumulator voltage
  successRead = 0;    // Variable integer to keep if we have Successful Read from Reader
  mode = 0;
  timerFlag = false;
  equalsMaster = false;  
  currCard = 0; 
  sendCard = 0;
  prevCard = 0;
  nextCard = 0;

  digitalWrite(LIGHT_PIN, 0);
  digitalWrite(LIGHT2_PIN, 1);  

  reader.PCD_Init();
}

void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = reader.PCD_ReadRegister(reader.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown),probably a chinese clone?"));
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
    Serial.println(F("SYSTEM HALTED: Check connections."));
    while (true); // do not go further
  }
}

uint8_t getID() {
  // Getting ready for Reading PICCs
  if (!reader.PICC_IsNewCardPresent()) { //If a new PICC placed to RFID reader continue
    return 0;
  }
  if (!reader.PICC_ReadCardSerial()) {   //Since a PICC placed get Serial and continue
    return 0;
  }
  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.println(F("Scanned PICC's UID:"));
  for ( uint8_t i = 0; i < 4; i++) {  //
    readCard[i] = reader.uid.uidByte[i];
    Serial.print(readCard[i], HEX);
  }
  Serial.println("");
  reader.PICC_HaltA(); // Stop reading
  return 1;
}

//////////////////////////////////////// Read an ID from EEPROM //////////////////////////////
void readID( uint8_t number ) {
  uint8_t start = (number * 4 ) + 2;    // Figure out starting position
  for ( uint8_t i = 0; i < 4; i++ ) {     // Loop 4 times to get the 4 Bytes
    storedCard[i] = EEPROM.read(start + i);   // Assign values read from EEPROM to array
  }
}

///////////////////////////////////////// Add ID to EEPROM   ///////////////////////////////////
void writeID(byte a[], uint8_t start) {
  for ( uint8_t j = 0; j < 4; j++ ) {   // Loop 4 times
    EEPROM.write( start + j, a[j] );  // Write the array values to EEPROM in the right position
  }
}

///////////////////////////////////////// Equal two cards   ///////////////////////////////////
bool cardEquals(byte a[], byte b[]) {
  for (uint8_t k = 0; k < 4; k++) {   // Loop 4 times
//    Serial.print(a[k], HEX);
//    Serial.print(" ");
//    Serial.println(b[k], HEX);
    if (a[k] != b[k])     // IF a != b then set match = false, one fails, all fail
      return false;
  }
  return true;      // Return true
}


void scanFirstMasterCard() {
  do {
    successRead = getID();            // sets successRead to 1 when we get read from reader otherwise 0
  } while (!successRead);                  // Program will not go further while you not get a successful read

  writeID(readCard, 2);
  for ( uint8_t i = 0; i < 4; i++ ) {          // Read Master Card's UID from EEPROM
    firstMasterCard[i] = EEPROM.read(2 + i);    // Write it to masterCard
  }
  Serial.println(F("First Master Card Defined"));
}


void scanGameCards() {
  // turn off timer
  TIMSK2 = 0;
  
  for (uint8_t i = 0; i < 4; i++) {
    equalsMaster = false;
    do {
      do {
        successRead = getID();  // sets successRead to 1 when we get read from reader otherwise 0
      } while (!successRead);   //the program will not go further while you are not getting a successful read
  
      if (cardEquals(readCard, firstMasterCard)) {
        Serial.println(F("It is a first Master Card"));
        Serial.println(F("Scan another PICC to Define as Game Card"));
        equalsMaster = true;
      } 
      else {
        equalsMaster = false;
        for (uint8_t l = 0; l < i; l++) {
          if (cardEquals(readCard, gameCards[l])) {
              Serial.print(F("It is already defined Game Card "));
              Serial.println(l);
              Serial.println(F("Scan another PICC to Define as Game Card"));
              equalsMaster = true;
           }
           else {
             Serial.print("Not equals card # ");
             Serial.println(l);
           }
        }
      } 
      if (!equalsMaster) {
        writeID(readCard, 10 + i*4);
        for ( uint8_t j = 0; j < 4; j++ ) {          // Read Master Card's UID from EEPROM
          gameCards[i][j] = EEPROM.read(10 + i*4 + j);    // Write it to masterCard
        }
        Serial.print(i);
        Serial.println(F(" Game Card Defined"));
        equalsMaster = false;
      }
    } while (equalsMaster);
  }
  Serial.println(F("All Game Cards Defined"));
  //turn on timer
  TIMSK2 = 1 << TOIE2;
}
