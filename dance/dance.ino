#include <DFMiniMp3.h>
#include <RS485_q.h>
#include <EEPROM.h>

#define DEF_VOLUME 28

#define RX_PIN A0
#define TX_PIN A2
#define EN_PIN A1

#define CLOCK_PIN 13
#define DATA_PIN 11
#define LATCH_PIN 10 

#define SLED1_PIN 8 //6 //r
#define SLED2_PIN 6 //7 //y
#define SLED3_PIN 9 //8 //green
#define SLED4_PIN 7 //9 //blue

#define OKLED_PIN 5
#define WRLED_PIN 4

#define OPTO_PIN 12
#define LIGHT_PIN 5
#define WHEEL_PIN 4

#define START_PIN 2
#define STOP_PIN 3

#define STEP1_PIN A4 //g
#define STEP2_PIN A6 //r
#define STEP3_PIN A7 //b
#define STEP4_PIN A5 //y
#define LOCK_PIN A3

#define TIME_STEP 522
#define TIME_SHIFT 300
#define STEPS  83
#define DEBOUNCE 10

uint8_t score = 0;
uint8_t reg0 = 0x00;
uint8_t reg1 = 0x00;
uint8_t reg2 = 0x00;
uint8_t reg3 = 0x00;
uint32_t reg = 0;

uint8_t data[STEPS];
uint8_t state = 0;
uint8_t step = 0;
uint8_t stepState = 0;
uint8_t tries = 0;
bool isLocked = true;

uint8_t volume = DEF_VOLUME;

uint32_t stepDelay = 0;

byte * rs485data;
const byte deviceName[] = "dance.";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdLock[] =   "lock..";
const byte cmdUnlock[] = "unlock";
const byte cmdReset[] =  "reset.";
const byte cmdStart[] =  "start.";
const byte cmdStop[] =   "stop..";
const byte cmdOn[] =     "turnon";
const byte cmdOff[] =    "trnoff";
const byte cmdReboot[] = "reboot";
const byte cmdVolUp[] =  "volup.";
const byte cmdVolDown[] ="voldn.";
byte reply[6];

//maintimer
static volatile uint32_t TimeMs = 0;

// implement a notification class,
// its member methods will get called 
class Mp3Notify {
public:
  static void OnError(uint16_t errorCode)
  {
    // see DfMp3_Error for code meaning
    Serial.println();
    Serial.print("Com Error ");
    Serial.println(errorCode);
  }

  static void OnPlayFinished(uint16_t globalTrack)
  {
    Serial.println();
    Serial.print("Play finished for #");
    Serial.println(globalTrack);   
  }

  static void OnCardOnline(uint16_t code)
  {
    Serial.println();
    Serial.print("Card online ");
    Serial.println(code);     
  }

  static void OnCardInserted(uint16_t code)
  {
    Serial.println();
    Serial.print("Card inserted ");
    Serial.println(code); 
  }

  static void OnCardRemoved(uint16_t code)
  {
    Serial.println();
    Serial.print("Card removed ");
    Serial.println(code);  
  }
};

DFMiniMp3<HardwareSerial, Mp3Notify> mp3(Serial);

void setup() {

  setupData();
  setupHardware();

  if (EEPROM.read(0) != 0x99) {
    EEPROM.update(1, volume);
    EEPROM.write(0, 0x99);
  }
  else {
    volume = EEPROM.read(1);
  }
  if (volume > 30) {
    volume = DEF_VOLUME;
    EEPROM.write(1, volume);
  }

  Serial.begin(9600);
  mp3.begin();
  mp3.setVolume(volume);
  mp3.setRepeatPlay(false);
 
  shiftClear();
  // Serial.println("On");

  TCCR2A = 1 << WGM21; // CTC mode ON 
  TCCR2B = 1 << CS22 | 0 << CS21 | 1 << CS20; // 101 = /128 = 125 000 Hz
  TIMSK2 = 1 << OCIE2A; // Outpit Compare interrupt 2
  OCR2A = 125; // 1000 Hz, interrupt every 1 ms exactly

  rs485.begin();
}


void loop() {
  // put your main code here, to run repeatedly:

  if (rs485.update()) {
    rs485data = rs485.getData();
    processCommand();
    rs485.reset(); 
  }

  if (state == 0) {
    processStepLights();
    
    if (!checkCoin())
      return;
    else {
      state = 1;
      lightOn();
      showHello();
    }
  }

  if (state == 1) {
    processStepLights();

    // process restart
    if (checkCoin()) {
      ledsOn();
      delay(200);
      ledsOff();      
      resetGame();
      delay(1000); // для исключения повторного срабатывания. костыль.
    }    

    if (checkStartButton()) {
      state = 2;
      startGame();
    }
  }


  if (state == 2) {    
    game();
    if (checkStopButton()) {
      endGame();
    }
  }

  if (state == 3) {    
    if (checkStopButton() or checkCoin()) {
      ledsOn();
      delay(200);
      ledsOff();
      resetGame();
      delay(1000); // для исключения повторного срабатывания. костыль.
    }
  }  

}

void processCommand() {
  if (compareCmd(rs485data, cmdLock)) {
    lockOn();
  }
  else if (compareCmd(rs485data, cmdUnlock)) {
    lockOff();
  }
  else if (compareCmd(rs485data, cmdStart)) {
    if (state == 1) {
      state = 2;
      startGame();
    }
  }
  else if (compareCmd(rs485data, cmdStop)) {
    if (state == 2) {
      endGame();
    }
  }
  else if (compareCmd(rs485data, cmdOn)) {
    if (state == 0) {
      state = 1;
      lightOn();
      showHello();
    }
  }    
  else if (compareCmd(rs485data, cmdOff)) {
    if (state != 0) {
      resetGame();
    }
  }  
  else if (compareCmd(rs485data, cmdVolUp)) {
    if (volume < 30) {
      volume++;
      mp3.setVolume(volume);
      EEPROM.update(1, volume);
    }
  }
  else if (compareCmd(rs485data, cmdVolDown)) {
    if (volume > 1) {
      volume--;
      mp3.setVolume(volume);
      EEPROM.update(1, volume);
    }
  }

  if (compareCmd(rs485data, cmdReset)) {
    resetGame();
    rs485.sendMsg((const byte*) "re-set", 6);
  }
  else {
    constructReply();
    rs485.sendMsg((const byte*) reply, 6);
  }  

}

void constructReply() {
  reply[0] = 's';
  reply[1] = state + 48; // convert number to ascii digit
  reply[2] = 'l';
  reply[3] = (isLocked ? '1' : '0');
  reply[4] = 't';
  reply[5] = tries + 48;
}

void game() {

  switch (stepState) { 
    case 0: // start step
      stepState++;
      ledsOff();

      if (score == 0) 
        lockOff();

      if (step <= 15) {
        ledsOn();
        stepDelay = mainTimerSet(50);
        stepState = 3;
      }
      else  {
        digitalWrite(SLED1_PIN,  data[step] & 0b00000001);
        digitalWrite(SLED2_PIN, (data[step] >> 1) & 0b00000001);
        digitalWrite(SLED3_PIN, (data[step] >> 2) & 0b00000001);
        digitalWrite(SLED4_PIN, (data[step] >> 3) & 0b00000001); 
        stepDelay = mainTimerSet(TIME_STEP);
      }
      break;

    case 1:
      if (step > 15) {
        if (checkStep(step)) {
          stepState++;
          updateScore(false); // down
        }
      }
      // no break

    case 2:
      if (mainTimerIsExpired(stepDelay)) {
        step++;
        stepState = 0;
        if (step == STEPS) {
          endGame();
        }
      }
      break;

    case 3:
      if (mainTimerIsExpired(stepDelay)) {
        if (step == 15)
          stepDelay = mainTimerSet(TIME_STEP - (50)); // + TIME_SHIFT)); // no time shift
        else
          stepDelay = mainTimerSet(TIME_STEP - 50);
        ledsOff();
        stepState = 2;
      }
      break;
  }
}

bool checkStep(uint8_t n) {
  static uint8_t buttons1 = 0;
  static uint8_t buttons2 = 0;
  static uint32_t timeDelay = 0;
  static bool prevFlag = false;

  if (n == 16)
    prevFlag = false;

  if (timeDelay == 0)
    timeDelay = mainTimerSet(10);

  if (mainTimerIsExpired(timeDelay)) {
    buttons1 = buttons2;
    buttons2 = checkStepButtons();
    if 
    ((buttons2 != 0) && (buttons2 == buttons1) &&
//     (((buttons2 == data[n-1]) && (mainTimerRemainingMs(stepDelay) > TIME_SHIFT) &&  not prevFlag) || 
//      ((buttons2 == data[n]) && (mainTimerRemainingMs(stepDelay) <= TIME_SHIFT) ))
     (((buttons2 == data[n-1]) && (mainTimerRemainingMs(stepDelay) > TIME_SHIFT) &&  not prevFlag) || 
      ((buttons2 == data[n]) ))
    ) {
      if (buttons2 == data[n])
        prevFlag = true;
      else
        prevFlag = false;
      return true;
    }
    timeDelay = mainTimerSet(10);
  }
  return false;
}

uint8_t checkStepButtons() {
  uint8_t b = 0;
  if (digitalRead(STEP1_PIN) == 1) // pressed
    b |= 1;
  if (analogRead(STEP2_PIN) > 500) // A6 & A7 don't work as digital pins
    b |= (1 << 1);
  if (analogRead(STEP3_PIN) > 500) // pressed
    b |= (1 << 2);
  if (digitalRead(STEP4_PIN) == 1) // pressed
    b |= (1 << 3);
  return b;
}

void startGame() {
//  for (uint8_t i = 0; i < STEPS; i++) {
//    data[i] &= ~(1 << 4); // reset 5th bit
//  }
  shiftClear();
  if (tries < 6) 
    score = 32 - (4 * tries);
  else 
    score = 8;
  step = 0;
  showScore();
  wheelOn();
  showCountdown();
  mp3.playMp3FolderTrack(1);  // sd:/mp3/0001.mp3
  // Serial.println("ST");
}

void endGame() {
  mp3.stop();
  wheelOff();
  tries++;
  
  if (score == 0) {
    showWin();
    lockOff();
    delay(1000);
    resetGame();
    lockOff();
    state = 3;
  } 
  else {
    showLose();
    state = 1;
  }
}

void resetGame() {
  mp3.stop();
  wheelOff();
  lightOff();
  ledsOff();
  lockOn();
  shiftClear();
  tries = 0;
  score = 0;

  state = 0;
}

/*
* ------------------------------------------------------------------------ 
*/

//Timer2 overflow interrupt vector handler
ISR(TIMER2_COMPA_vect) {
  TimeMs++;
}

uint32_t mainTimerGetMs(void) {
  return TimeMs;
}

bool mainTimerIsExpired(const uint32_t Timer) {
  return ((TimeMs - Timer) < (1UL << 31));
}

uint32_t mainTimerRemainingMs(const uint32_t Timer) {
  if ((TimeMs - Timer) > (1UL << 31))
    return (Timer - TimeMs);
  else
    return 0;
}

uint32_t mainTimerSet(const uint32_t AddTimeMs) {
  return TimeMs + AddTimeMs;
}

/*
* ------------------------------------------------------------------------ 
*/

void shiftClear() {
  digitalWrite(LATCH_PIN, 0);
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, 0x00);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, 0x00);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, 0x00);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, 0x00);  
  digitalWrite(LATCH_PIN, 1);   
}

void showScore() {// костыль
  reg0 = 0;
  reg1 = 0;
  reg2 = 0;
  reg3 = 0;
  uint8_t i = score;
  score = 0;
  for (; i > 0; i--)
    updateScore(true);
}

void updateScore(boolean up) {
  if (up) {  
    if (score < 8) {
      reg0 |= (1 << (score));
    }
    else if (score < 16) {
      reg1 |= (1 << (score - 8));
    }
    else if (score < 24) {
      reg2 |= (1 << (score - 16));
    }
    else {
      reg3 |= (1 << (score - 24));
    }
    if (score < 32)
      score++;
  }
  else
  {
    if (score < 9) {
      reg0 &= ~(1 << (score - 1));
    }
    else if (score < 17) {
      reg1 &= ~(1 << (score - 9));
    }
    else if (score < 25) {
      reg2 &= ~(1 << (score - 17));
    }
    else {
      reg3 &= ~(1 << (score - 25));
    }
    if (score > 0)
      score--;
  }

  digitalWrite(LATCH_PIN, 0);
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, reg3);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, reg2);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, reg1);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, reg0);  
  digitalWrite(LATCH_PIN, 1);  
}

void updateScoreN(boolean up) {
  if (up) {
    reg |= (1 << score);
  }
  else {
    reg &- ~(1 << (score - 1));
  }

  digitalWrite(LATCH_PIN, 0);
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, (reg >> 24) & 0xFF);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, (reg >> 16) & 0xFF);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, (reg >> 8) & 0xFF);  
  shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST,  reg & 0xFF);  
  digitalWrite(LATCH_PIN, 1);

}

void nb_delay_1(uint16_t delay_ms) {
  for (uint16_t i = 0; i < delay_ms; i++) {
    if (checkStopButton())
    {
      endGame();
      break;
    }
    delay(1);
  }
}

void showHello() {
  digitalWrite(SLED2_PIN, 1);
  nb_delay_1(100);
  digitalWrite(SLED1_PIN, 1);
  nb_delay_1(100);
  digitalWrite(SLED3_PIN, 1);
  nb_delay_1(100);
  digitalWrite(SLED4_PIN, 1);  
  nb_delay_1(200);
  digitalWrite(SLED4_PIN, 0);
  nb_delay_1(100);
  digitalWrite(SLED3_PIN, 0);
  nb_delay_1(100);
  digitalWrite(SLED1_PIN, 0);
  nb_delay_1(100);
  digitalWrite(SLED2_PIN, 0);  
  nb_delay_1(100);  
  for (uint8_t i = 0; i < 5; i++) {
    digitalWrite(SLED1_PIN, 1);
    digitalWrite(SLED2_PIN, 1);
    digitalWrite(SLED3_PIN, 1);
    digitalWrite(SLED4_PIN, 1);      
    nb_delay_1(200);
    digitalWrite(SLED1_PIN, 0);
    digitalWrite(SLED2_PIN, 0);
    digitalWrite(SLED3_PIN, 0);
    digitalWrite(SLED4_PIN, 0);      
    nb_delay_1(200);
  }

  for (uint8_t i = 0; i < 32; i++) {
    updateScore(true);
    nb_delay_1(30);
  }

  for (uint8_t i = 0; i < 32; i++) {
    updateScore(false);
    nb_delay_1(30);
  }
  shiftClear();
}

void showCountdown() {
  digitalWrite(SLED2_PIN, 1);
  digitalWrite(SLED1_PIN, 1);
  digitalWrite(SLED3_PIN, 1);
  digitalWrite(SLED4_PIN, 1);    
  nb_delay_1(TIME_STEP);
  digitalWrite(SLED4_PIN, 0);
  nb_delay_1(TIME_STEP);
  digitalWrite(SLED3_PIN, 0);
  nb_delay_1(TIME_STEP);
  digitalWrite(SLED1_PIN, 0);
  nb_delay_1(TIME_STEP);
  digitalWrite(SLED2_PIN, 0);  
  nb_delay_1(TIME_STEP);   
}

void showWin() {
  for (uint8_t i = 0; i < 3; i++) {
    ledsOn();     
    nb_delay_1(250);
    ledsOff();  
    nb_delay_1(250);
  }
}

void showLose() {
  uint8_t t = 32 - score;
  for (uint8_t i = 0; i < t; i++) {
    updateScore(true);
    nb_delay_1(30);
  }
  ledsOn();  
  nb_delay_1(400);
  digitalWrite(SLED1_PIN, 0); // g
  digitalWrite(SLED3_PIN, 0); // b
  nb_delay_1(600);
  digitalWrite(SLED2_PIN, 0); // r
  digitalWrite(SLED4_PIN, 0); // y
}

bool checkCoin() {
  static uint32_t timeDelay = 0;
  static uint8_t pinState = 1; // off
  if (mainTimerIsExpired(timeDelay)) {
    if (digitalRead(OPTO_PIN) == 0) {
      if (pinState == 0)
        return true;
      else
        pinState = 0;
    }
    else
      pinState = 1;
    timeDelay = mainTimerSet(5);
  }
  return false;
}

bool checkStartButton() {
  static uint32_t timeDelay = 0;
  static uint8_t pinState = 1; // off
  if (mainTimerIsExpired(timeDelay)) {
    if (digitalRead(START_PIN) == 0) {
      if (pinState == 0)
        return true;
      else
        pinState = 0;
    }
    else
      pinState = 1;
    timeDelay = mainTimerSet(10);
  }
  return false;
}

bool checkStopButton() {
  static uint32_t timeDelay = 0;
  static uint8_t pinState = 1; // off
  if (mainTimerIsExpired(timeDelay)) {
    if (digitalRead(STOP_PIN) == 0) {
      if (pinState == 0)
        return true;
      else
        pinState = 0;
    }
    else
      pinState = 1;
    timeDelay = mainTimerSet(10);
  }
  return false;
}

inline void lightOn() {
  digitalWrite(LIGHT_PIN, HIGH);
}

inline void lightOff() {
  digitalWrite(LIGHT_PIN, LOW);
}

inline void wheelOn() {
  digitalWrite(WHEEL_PIN, LOW);
}

inline void wheelOff() {
  digitalWrite(WHEEL_PIN, HIGH);
}

inline void lockOn() {
  digitalWrite(LOCK_PIN, LOW);
  isLocked = true;
}

inline void lockOff() {
  digitalWrite(LOCK_PIN, HIGH);
  isLocked = false;
}


inline void ledsOff() {
  digitalWrite(SLED1_PIN, 0);
  digitalWrite(SLED2_PIN, 0);
  digitalWrite(SLED3_PIN, 0);
  digitalWrite(SLED4_PIN, 0);
}

inline void ledsOn() {
  digitalWrite(SLED1_PIN, 1);
  digitalWrite(SLED2_PIN, 1);
  digitalWrite(SLED3_PIN, 1);
  digitalWrite(SLED4_PIN, 1);
}

void  processStepLights() {
  if (digitalRead(STEP1_PIN) == 1) // pressed
    digitalWrite(SLED1_PIN, 1);
  else
    digitalWrite(SLED1_PIN, 0);
  if (analogRead(STEP2_PIN) > 500) // A6 & A7 don't work as digital pins
    digitalWrite(SLED2_PIN, 1);
  else
    digitalWrite(SLED2_PIN, 0);
  if (analogRead(STEP3_PIN) > 500) // pressed
    digitalWrite(SLED3_PIN, 1);
  else
    digitalWrite(SLED3_PIN, 0);
  if (digitalRead(STEP4_PIN) == 1) // pressed
    digitalWrite(SLED4_PIN, 1);
  else
    digitalWrite(SLED4_PIN, 0);
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}



void setupHardware() {
  pinMode(LATCH_PIN, OUTPUT);
  pinMode(DATA_PIN,  OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);

  pinMode(SLED1_PIN, OUTPUT);
  pinMode(SLED2_PIN, OUTPUT);
  pinMode(SLED3_PIN, OUTPUT);
  pinMode(SLED4_PIN, OUTPUT);
  // pinMode(OKLED_PIN, OUTPUT);
  // pinMode(WRLED_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(WHEEL_PIN, OUTPUT);

  pinMode(OPTO_PIN, INPUT);

  pinMode(START_PIN, INPUT);
  pinMode(STOP_PIN,  INPUT);  
  pinMode(STEP1_PIN, INPUT);
  pinMode(STEP2_PIN, INPUT);
  pinMode(STEP3_PIN, INPUT);
  pinMode(STEP4_PIN, INPUT);

  pinMode(LOCK_PIN, OUTPUT);
  digitalWrite(LOCK_PIN, LOW);

  digitalWrite(LIGHT_PIN, LOW);
  digitalWrite(WHEEL_PIN, HIGH);
  digitalWrite(SLED1_PIN, 0);
  digitalWrite(SLED2_PIN, 0);
  digitalWrite(SLED3_PIN, 0);
  digitalWrite(SLED4_PIN, 0);  
}

void setupData() {
  data[0 ] = 0b10000010; // 8th bit set = ignore pattern
  data[1 ] = 0b10000100;
  data[2 ] = 0b10000010;
  data[3 ] = 0b10000100;
  data[4 ] = 0b10000010;
  data[5 ] = 0b10000100;
  data[6 ] = 0b10000010;
  data[7 ] = 0b10000100;

  data[8 ] = 0b10000001; // 8th bit set = ignore pattern
  data[9 ] = 0b10001000;
  data[10] = 0b10000001;
  data[11] = 0b10001000;
  data[12] = 0b10000001;
  data[13] = 0b10001000;
  data[14] = 0b10000001;
  data[15] = 0b10001000;  

  data[16] = 0b00000010;
  data[17] = 0b00000000;
  data[18] = 0b00000100;
  data[19] = 0b00000000;
  data[20] = 0b00000010;
  data[21] = 0b00000000;
  data[22] = 0b00000000;
  data[23] = 0b00000000;

  data[24] = 0b00001000;
  data[25] = 0b00000000;
  data[26] = 0b00000001;
  data[27] = 0b00000000;
  data[28] = 0b00000100;
  data[29] = 0b00000000;
  data[30] = 0b00000000;
  data[31] = 0b00000000;

  data[32] = 0b00000001;
  data[33] = 0b00000100;
  data[34] = 0b00000001;
  data[35] = 0b00000100;
  data[36] = 0b00000010;
  data[37] = 0b00001000;
  data[38] = 0b00000010;
  data[39] = 0b00001000;

  data[40] = 0b00000001;
  data[41] = 0b00000000;
  data[42] = 0b00000100;
  data[43] = 0b00000000;
  data[44] = 0b00001010;
  data[45] = 0b00000000;
  data[46] = 0b00000000;
  data[47] = 0b00000000;

  data[48] = 0b00001000;
  data[49] = 0b00000000;
  data[50] = 0b00000010;
  data[51] = 0b00000000;
  data[52] = 0b00000100;
  data[53] = 0b00000001;
  data[54] = 0b00000100;
  data[55] = 0b00000001;

  data[56] = 0b00000010;
  data[57] = 0b00000000;
  data[58] = 0b00001000;
  data[59] = 0b00000000;
  data[60] = 0b00000001;
  data[61] = 0b00000100;
  data[62] = 0b00000001;
  data[63] = 0b00000100;
  
  data[64] = 0b00001010;
  data[65] = 0b00000000;
  data[66] = 0b00000000;
  data[67] = 0b00000000;
  data[68] = 0b00000101;
  data[69] = 0b00000000;
  data[70] = 0b00000000;
  data[71] = 0b00000000;

  data[72] = 0b00000010;
  data[73] = 0b00000001;
  data[74] = 0b00000100;
  data[75] = 0b00001000;
  data[76] = 0b00000101;
  data[77] = 0b00001010;
  data[78] = 0b00000101;
  data[79] = 0b00001010;

  data[80] = 0b00000000;
  data[81] = 0b00000000;
  data[82] = 0b00000000;

  // data[83] = 0;
}
