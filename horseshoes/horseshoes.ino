#include <RS485_q.h>

#define LOCK_PIN 2

#define RX_PIN A0
#define TX_PIN A1
#define EN_PIN A2

byte * rs485data;
const byte deviceName[] = "horse.";
RS485 rs485(deviceName, EN_PIN, RX_PIN, TX_PIN);

const byte cmdStatus[] = "status";
const byte cmdLock[] =   "lock..";
const byte cmdUnlock[] = "unlock";
const byte cmdReset[] =  "reset.";
const byte cmdReboot[] = "reboot";
byte reply[6];

bool isOpened = false;

uint8_t timerCompare = 10;
uint8_t timerCnt = 0;
uint8_t checkCounts = 0;
#define CHECK_COMP 10
//uint8_t stateMask = 0;
//uint8_t prevStateMask = 0;
uint8_t inPins [5] = {3,  4,  5,  6, 7};
uint8_t outPins[5] = {12, 11, 10, 9, 8};
//uint8_t toolPokesNeeded[4] = {1, 2, 3, 4}; 
//uint16_t lastPokeTime = 0, timeDiff = 0;

void setup() {
  // put your setup code here, to run once:

  for (uint8_t i = 0; i < 5; i++) {
    pinMode(inPins[i], INPUT);
    pinMode(outPins[i], OUTPUT);
    digitalWrite(outPins[i], LOW);
  }

  pinMode(LOCK_PIN, OUTPUT);
  digitalWrite(LOCK_PIN, LOW);

  Serial.begin(115200);
  Serial.println("Start");

  //Установка Таймера2.
  //Конфигурирует 8-битный Таймер2 ATMega168 для выработки прерывания
  //с заданной частотой.
  TCCR2A = 0;
  TCCR2B = 1<<CS22 | 0<<CS21 | 1<<CS20; // 101 = /1024
  
  //Подключение прерывания по переполнению Timer2
  TIMSK2 = 1<<TOIE2;


}

void loop() {
 if (rs485.update()) {
    rs485data = rs485.getData();
    #ifdef DEBUG
    Serial.print("Got data: ");
    Serial.write(rs485data, 6);
    Serial.println();
    #endif
    if (compareCmd(rs485data, cmdLock)) {
      lockClose();
    }
    else if (compareCmd(rs485data, cmdUnlock)) {
      lockOpen();
    }    

    if (compareCmd(rs485data, cmdReset)) {
      resetGame();
      rs485.sendMsg((const byte*) "re-set", 6);
    }
    else {
      constructReply();
      rs485.sendMsg((const byte*) reply, 6);
    }
  rs485.reset();
  }    
}

bool checkPin(uint8_t n) {
  bool result = true;
  digitalWrite(outPins[n], HIGH);
  delayMicroseconds(10);
  for (uint8_t i = 0; i < 5; i++) {
    if (i == n) {
      if (digitalRead(inPins[i]) != HIGH)
        result = false;
    }
    else {
      if (digitalRead(inPins[i]) != LOW)
        result = false;
    }
  }
  digitalWrite(outPins[n], LOW);
  return result;
}

ISR(TIMER2_OVF_vect) {
  if (timerCnt < timerCompare) {
    timerCnt++;
  } else {
    timerCnt = 0;

    bool allCorrect = true;
    
    for (uint8_t i = 0; i < 5; i++) {
      if (checkPin(i) == false) {
        allCorrect = false;  
        Serial.print("0");
      }
      else {
        Serial.print("1");
      }
    }

    Serial.println();

    if (allCorrect) {
      checkCounts++;
    }
    else {
      if (isOpened) {
        lockClose();
      }
    }

    if (checkCounts >= CHECK_COMP && !isOpened) {
      checkCounts = 0;
      lockOpen();
    }

    
  }
}

void lockOpen() {
  isOpened = true;
  digitalWrite(LOCK_PIN, HIGH);
  Serial.println("Opened!");
}

void lockClose() {
  isOpened = false;
  digitalWrite(LOCK_PIN, LOW);
  Serial.println("Closed!");  
}

bool compareCmd(byte* one, const byte* two)
{
  for (uint8_t i = 0; i < 6; i++) {
    if (one[i] != two[i])
      return false;
  }
  return true;
}

void constructReply() {
  if (isOpened) {
    reply = "opened";
  } 
  else {
    reply = "closed";
  }
}